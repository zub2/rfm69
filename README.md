# rfm69
This is a Linux library and command line tools that can talk to a [RFM69](https://www.hoperf.com/modules/rf_transceiver/index.html) module. Currently only SPI via a FTDI USB chip with MPSSE (running SPI over it) is supported. Eventually I'd like to also add spidev linux support.

The MPSSE/SPI part is based on [ft232-test](https://gitlab.com/zub2/ft232-test).

## Resources
* [RFM69](https://www.hoperf.com/modules/rf_transceiver/index.html)
* [libftdi](https://www.intra2net.com/en/developer/libftdi/index.php)
* [MPSSE on FTDI chips](https://gitlab.com/zub2/ft232-test)

## Compiling
You need [meson](http://mesonbuild.com/), [ninja](https://ninja-build.org/), [libftdi](https://www.intra2net.com/en/developer/libftdi/index.php) and a C++ compiler that supports C++ 17. Most likely you can get libftdi from you distro package system, so you don't have to build it yourself.

## Running

Typically you will need to run this program as root unless the user that runs it had the right to access the raw USB device.

Alternatively, create a `ftdi.rules` file in `/etc/udev/rules.d` with the following content:

```
SUBSYSTEMS=="usb", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6014", MODE="0660", GROUP="plugdev"
```

If needed, adjust the IDs and/or the group. You can see the USB vendor and product IDs in the output of `lsusb`. Use `udevadm control --reload-rules && udevadm trigger` to make udev notice the change.

Currently there are two tools provided: `rfm69-ctl` and `rfm69-ook-transmit`.

### Specifying the device to talk to
When using the command line tools or the libray, you need to specify what device to talk to.

Currently, only SPI over FTDI chips (via [libftdi](https://www.intra2net.com/en/developer/libftdi/index.php)) is supported.

#### SPI over libftdi
The device name is passed as-is to `ftdi_usb_open_string` (from [libftdi](https://www.intra2net.com/en/developer/libftdi/index.php)). Accepted formats are described in [libftdi documentation](https://www.intra2net.com/en/developer/libftdi/documentation/group__libftdi.html) (search for `ftdi_usb_open_string`).

There are some options that you can configure:
* FTDI chip interface. This makes sense for chips that have multiple interfaces. Can be one of `A`, `B`, `C` or `D`, or `ANY`, letting `libftdi` decide.
* SPI clock frequency. The RFM69 requires clock frequency ≤ 10 MHz.

### rfm69-ctl
This tool can be used to read and write registers of the RFM69. It can also decode raw values. It requires the name of the device (see above) and one or more commands.

The following commands are available:
* read register: `-r reg`
* write register: `-w reg=value`
* dump all registers: `-d`

You can use either register numbers (e.g. `1`) or register names (e.g. `RegOpMode`). For `value`, you can use either decadic (`12`) or hex (`0xc`) numbers. You can specify multiple command and they will be executed in sequence.

Example:
```
$ rfm69-ctl i:0x0403:0x6014 -r 1 -w 1=0 -r 1
register RegOpMode (0x01): 4 (0x04, 0b00000100)
SequencerOff: 0
ListenOn: 0
ListenAbort: 0
Mode: STDBY (0b001)

setting register RegOpMode (0x01) to 0 (0x00, 0b00000000)

register RegOpMode (0x01): 0 (0x00, 0b00000000)
SequencerOff: 0
ListenOn: 0
ListenAbort: 0
Mode: SLEEP (0b000)
```

### rfm69-ook-transmit
This tool can be used to experiment with RFM69 transmit settings and perform actual OOK transmission. I've used it to figure out what exactly is the RFM69 transmitting in various settings (together with a [librtlsdr](https://osmocom.org/projects/rtl-sdr/wiki/Rtl-sdr) device and [some octave scripts](https://gitlab.com/zub2/octave-somfy)).

You can just run it with `-h` to see its options. Like `rfm69-ctl`, it needs to be told the device to use (see above). Then it accepts options to configure RFM69 packet format and the actual data to send.

Example:

```
$ rfm69-ook-transmit i:0x0403:0x6014 0xaa
RFM69 rev. 2 (mask rev. 4)
bitRate=3200
frequency=433000000
Broadcasting:
	* preamble: 1 bit (1)
	* sync word: none
	* payload: 0xaa
```

You can use argument `-v` to make it more verbose.

## TODOs
These tools were made to scratch an itch. So it does care only about some aspects of the RFM69. E.g. there's no tool for receiving and the `rfm69-ctl` tool doesn't (yet?) decode some registers useful in RX mode.

Another thing missing is Linux spidev support (useful when connecting the RFM69 module to a real SPI).
