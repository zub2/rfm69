/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RFM69_IRFM69IO_H
#define RFM69_IRFM69IO_H

#include <cstdint>
#include <vector>

class IRFM69IO
{
public:
	/**
	 * Read a single RFM69 register.
	 *
	 * @param[in] r Register number. You can use one of the Reg* constants.
	 * @return Value of the register.
	 */
	virtual uint8_t readRegister(uint8_t r) = 0;

	/**
	 * Read a range of RFM69 registers.
	 *
	 * This reads registers in the range [r, r+count-1]. Except when RegFifo is read.
	 * For RegFifo, count values are read from it.
	 *
	 * @param[in] r The first register to read. You can use one of the Reg* constants.
	 * @param[in] count Number of registers to read.
	 *
	 * @return Values of the registers.
	 */
	virtual std::vector<uint8_t> readRegisters(uint8_t r, size_t count) = 0;

	/**
	 * Write a single RFM69 register.
	 *
	 * @param[in] r Register number. You can use one of the Reg* constants.
	 * @param[in] value Value of store in the register.
	 */
	virtual void writeRegister(uint8_t r, uint8_t value) = 0;

	/**
	 * Write a range of RFM69 registers.
	 *
	 * This writes registers in the range [r, r+count-1]. Except when RegFifo is written.
	 * For RegFifo, count values are written to it.
	 *
	 * @param[in] r The first register to write. You can use one of the Reg* constants.
	 * @param[in] values Pointer to an array holding the values to store.
	 * @param[in] count Number of registers to read.
	 *
	 * @return Values of the registers.
	 */
	virtual void writeRegisters(uint8_t r, const uint8_t * values, size_t count) = 0;

	virtual ~IRFM69IO() = default;
};

#endif // RFM69_IRFM69IO_H
