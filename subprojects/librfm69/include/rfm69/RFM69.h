/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RFM69_RFM69_H
#define RFM69_RFM69_H

#include <cstdint>
#include <utility>
#include <vector>
#include <memory>

#include <ftdi.h>

#include "IRFM69IO.h"

class ISPI;

/**
 * Class that can control a RFM69 module.
 *
 * It provides low-level access: reading and writing RFM69 registers, but also some higher-level
 * functions are provided to make setting the registers easier.
 */
class RFM69
{
public:
	enum class Mode
	{
		Sleep,
		StandBy,
		Transmit
	};

	enum class PacketFormat
	{
		FixedLength,
		VariableLength
	};

	enum class DCFree
	{
		None,
		Manechester,
		Whitening
	};

	enum class AddressFiltering
	{
		None,
		MatchNodeAddr,
		MatchNodeAddrOfBroadcast
	};

	enum class OOKModulationShaping
	{
		None,
		BR,
		DoubleBR
	};

	enum class TxStartCondition
	{
		FifoLevel,
		FifoNotEmpty
	};

	/**
	 * Create an instance that talks to a RFM69 module that is connected via a FTDI USB chip.
	 *
	 * This works for FTDI USB serial chips that implement the MPSSE, like the FT232H.
	 *
	 * @param[in] device Device name, passed to ftdi_usb_open_string. Can be e.g. 'i:vendor:product'.
	 * @param[in] iface FTDI chip interface. Useful for chips that have more than 1 MPSSE interfaces.
	 * @param[in] clockFrequency SPI clock frequency in Hz. FT232H can support up to 10MHz but
	 * if you have the SPI wires all over your breadboard, better stick to something smaller.
	 * @param[in] highPower Pass true when the device is a high-power one (RFM69HW, RFM69HCW). It's
	 * needed to be able to set the power level correctly.
	 * @param[in] verbose Be verbose. When set to true, RFM69 register accesses (read and writes)
	 * are logged. This can be useful for debugging.
	 */
	RFM69(const char * device, ftdi_interface iface, unsigned clockFrequency, bool highPower, bool verbose);

	RFM69(std::string && readCommand, std::string && writeCommand, bool highPower, bool verbose);

	~RFM69();

	std::pair<unsigned, unsigned> getVersion();

	void setMode(Mode mode);
	void setBitRate(uint16_t bitrate);
	void setFrequency(uint32_t frequency);
	void disableSyncBytes();
	void setSyncBytes(const std::vector<uint8_t> & syncBytes);
	void setPacketConfig(PacketFormat packetFormat, DCFree dcFree, bool crc, bool autoClearOnCrcError,
			AddressFiltering addressFiltering);
	void setPacketLength(uint8_t length);
	void setPacketOOKMode(OOKModulationShaping shaping);
	void disableClkOut();
	void setPreambleSize(uint16_t preambleSize);
	void setFifoTresh(TxStartCondition txStartCondition, uint8_t fifoThreshold);

	/**
	 * Set output power level.
	 *
	 * For non-high power devices (RFM69W, RFM69CW), only the PA0 can be used
	 * and the valid range is -18 dBm to +13 dBm.
	 *
	 * For high-power devices (RFM69HW, RFM69HCW), PA0 cannot be used, but there
	 * are PA1 and PA2. The valid range is -2 dBm to +20 dBm.
	 *
	 * Based on work by André Hessling - https://github.com/ahessling/RFM69-STM32
	 *
	 * @param [in] level Desired power level in dBm.
	 */
	void setPowerLevel(int level);

	/**
	 * Read a single RFM69 register.
	 *
	 * @param[in] r Register number. You can use one of the Reg* constants.
	 * @return Value of the register.
	 */
	uint8_t readRegister(uint8_t r);

	/**
	 * Read a range of RFM69 registers.
	 *
	 * This reads registers in the range [r, r+count-1]. Except when RegFifo is read.
	 * For RegFifo, count values are read from it.
	 *
	 * @param[in] r The first register to read. You can use one of the Reg* constants.
	 * @param[in] count Number of registers to read.
	 *
	 * @return Values of the registers.
	 */
	std::vector<uint8_t> readRegisters(uint8_t r, size_t count);

	/**
	 * Write a single RFM69 register.
	 *
	 * @param[in] r Register number. You can use one of the Reg* constants.
	 * @param[in] value Value of store in the register.
	 */
	void writeRegister(uint8_t r, uint8_t value);

	/**
	 * Write a range of RFM69 registers.
	 *
	 * This writes registers in the range [r, r+count-1]. Except when RegFifo is written.
	 * For RegFifo, count values are written to it.
	 *
	 * @param[in] r The first register to write. You can use one of the Reg* constants.
	 * @param[in] values Pointer to an array holding the values to store.
	 * @param[in] count Number of registers to read.
	 *
	 * @return Values of the registers.
	 */
	void writeRegisters(uint8_t r, const uint8_t * values, size_t count);

	/**
	 * Wait for RegIrqFlags2 to enter desired state.
	 *
	 * This waits until (RegIrqFlags2 & mask) == value.
	 *
	 * @param[in] mask Mask that is applied to RegIrqFlags before comparison.
	 * @param[in] value Value that the masked RegIrqFlags2 is compared against.
	 *
	 * @todo Add timeout.
	 */
	void waitForIrqFlags2(uint8_t mask, uint8_t value);

	/**
	 * Constants from the RFM69 datasheet.
	 *
	 * See e.g. https://www.hoperf.com/data/upload/portal/20190306/RFM69HW-V1.3%20Datasheet.pdf
	 */
	static constexpr size_t FIFO_SIZE = 66;
	static constexpr float FXOSC = 32e6; // 32 MHz
	static constexpr float FSTEP = FXOSC/(2<<18);

	// RFM69 registers
	static constexpr uint8_t RegFifo = 0x00;
	static constexpr uint8_t RegOpMode = 0x01;
	static constexpr uint8_t RegDataModul = 0x02;
	static constexpr uint8_t RegBitrateMsb = 0x03;
	static constexpr uint8_t RegBitrateLsb = 0x04;
	static constexpr uint8_t RegFdevMsb = 0x05;
	static constexpr uint8_t RegFdevLsb = 0x06;
	static constexpr uint8_t RegFrfMsb = 0x07;
	static constexpr uint8_t RegFrfMid = 0x08;
	static constexpr uint8_t RegFrfLsb = 0x09;
	static constexpr uint8_t RegOsc1 = 0x0a;
	static constexpr uint8_t RegAfcCtrl = 0x0b;
	// Reserved0C
	static constexpr uint8_t RegListen1 = 0x0d;
	static constexpr uint8_t RegListen2 = 0x0e;
	static constexpr uint8_t RegListen3 = 0x0f;
	static constexpr uint8_t RegVersion = 0x10;
	static constexpr uint8_t RegPaLevel = 0x11;
	static constexpr uint8_t RegPaRamp = 0x12;
	static constexpr uint8_t RegOcp = 0x13;
	// Reserved14
	// Reserved15
	// Reserved16
	// Reserved17
	static constexpr uint8_t RegLna = 0x18;
	static constexpr uint8_t RegRxBw = 0x19;
	static constexpr uint8_t RegAfcBw = 0x1a;
	static constexpr uint8_t RegOokPeak = 0x1b;
	static constexpr uint8_t RegOokAvg = 0x1c;
	static constexpr uint8_t RegOokFix = 0x1d;
	static constexpr uint8_t RegAfcFei = 0x1e;
	static constexpr uint8_t RegAfcMsb = 0x1f;
	static constexpr uint8_t RegAfcLsb = 0x20;
	static constexpr uint8_t RegFeiMsb = 0x21;
	static constexpr uint8_t RegFeiLsb = 0x22;
	static constexpr uint8_t RegRssiConfig = 0x23;
	static constexpr uint8_t RegRssiValue = 0x24;
	static constexpr uint8_t RegDioMapping1 = 0x25;
	static constexpr uint8_t RegDioMapping2 = 0x26;
	static constexpr uint8_t RegIrqFlags1 = 0x27;
	static constexpr uint8_t RegIrqFlags2 = 0x28;
	static constexpr uint8_t RegRssiThresh = 0x29;
	static constexpr uint8_t RegRxTimeout1 = 0x2a;
	static constexpr uint8_t RegRxTimeout2 = 0x2b;
	static constexpr uint8_t RegPreambleMsb = 0x2c;
	static constexpr uint8_t RegPreambleLsb = 0x2d;
	static constexpr uint8_t RegSyncConfig = 0x2e;
	static constexpr uint8_t RegSyncValue1 = 0x2f;
	static constexpr uint8_t RegSyncValue2 = 0x30;
	static constexpr uint8_t RegSyncValue3 = 0x31;
	static constexpr uint8_t RegSyncValue4 = 0x32;
	static constexpr uint8_t RegSyncValue5 = 0x33;
	static constexpr uint8_t RegSyncValue6 = 0x34;
	static constexpr uint8_t RegSyncValue7 = 0x35;
	static constexpr uint8_t RegSyncValue8 = 0x36;
	static constexpr uint8_t RegPacketConfig1 = 0x37;
	static constexpr uint8_t RegPayloadLength = 0x38;
	static constexpr uint8_t RegNodeAdrs = 0x39;
	static constexpr uint8_t RegBroadcastAdrs = 0x3a;
	static constexpr uint8_t RegAutoModes = 0x3b;
	static constexpr uint8_t RegFifoThresh = 0x3c;
	static constexpr uint8_t RegPacketConfig2 = 0x3d;
	static constexpr uint8_t RegAesKey1 = 0x3e;
	static constexpr uint8_t RegAesKey2 = 0x3f;
	static constexpr uint8_t RegAesKey3 = 0x40;
	static constexpr uint8_t RegAesKey4 = 0x41;
	static constexpr uint8_t RegAesKey5 = 0x42;
	static constexpr uint8_t RegAesKey6 = 0x43;
	static constexpr uint8_t RegAesKey7 = 0x44;
	static constexpr uint8_t RegAesKey8 = 0x45;
	static constexpr uint8_t RegAesKey9 = 0x46;
	static constexpr uint8_t RegAesKey10 = 0x47;
	static constexpr uint8_t RegAesKey11 = 0x48;
	static constexpr uint8_t RegAesKey12 = 0x49;
	static constexpr uint8_t RegAesKey13 = 0x4a;
	static constexpr uint8_t RegAesKey14 = 0x4b;
	static constexpr uint8_t RegAesKey15 = 0x4c;
	static constexpr uint8_t RegAesKey16 = 0x4d;
	static constexpr uint8_t RegTemp1 = 0x4e;
	static constexpr uint8_t RegTemp2 = 0x4f;
	// 0x50+ are test registers
	static constexpr uint8_t RegTestLna = 0x58;
	static constexpr uint8_t RegTestPa1 = 0x5a;
	static constexpr uint8_t RegTestPa2 = 0x5c;
	static constexpr uint8_t RegTestDagc = 0x6f;
	static constexpr uint8_t RegTestAfc = 0x71;

	/*
	 * It's not obvious what is the last register. Any number up to 0x7f can be used.
	 * The highest register mentioned by RFM69 documentation is RegTestAfc, so let's
	 * assume it's the last one.
	 */
	static constexpr uint8_t RegLast = RegTestAfc;

private:
	void setSyncConfig(unsigned syncWordSize);

	/**
	 * Enable high output power mode (+20dBm mode) on high-power devices.
	 *
	 * This is needed to max out the output power. It must be disabled when
	 * receiving.
	 */
	void setHighPowerMode(bool enable);

	void writeBytes(const uint8_t * data, unsigned size);
	void writeAndReadBytes(const uint8_t * writeData, unsigned writeSize,
			std::vector<uint8_t> & readData, unsigned readSize);

	const std::unique_ptr<IRFM69IO> m_io;
	const bool m_highPower;
};

#endif // RFM69_RFM69_H
