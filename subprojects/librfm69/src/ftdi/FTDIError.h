/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RFM69_FTDI_ERROR_H
#define RFM69_FTDI_ERROR_H

#include <stdexcept>
#include <string>
#include <sstream>

#include <ftdi.h>

class FTDIError: public std::runtime_error
{
public:
	FTDIError(ftdi_context * context, const std::string & prefix):
		runtime_error(makeErrorString(context, prefix))
	{}

private:
	static std::string makeErrorString(ftdi_context * context, const std::string & prefix)
	{
		std::stringstream s;
		s << prefix << ": " << ftdi_get_error_string(context);

		return s.str();
	}
};

#endif // RFM69_FTDI_ERROR_H
