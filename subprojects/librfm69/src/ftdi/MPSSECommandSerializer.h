/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RFM69_MPSSE_COMMAND_SERIALIZER_H
#define RFM69_MPSSE_COMMAND_SERIALIZER_H

#include <vector>
#include <bitset>

#include "FTDIContext.h"
#include "FTDIError.h"

class FTDITransferControl
{
public:
	FTDITransferControl(FTDIContext & context, ftdi_transfer_control * tc):
		m_context(context),
		m_tc(tc)
	{}

	explicit operator bool() const
	{
		return m_tc != nullptr;
	}

	int finish()
	{
		const int r = finishImpl();
		if (r < 0)
			throw FTDIError(m_context, "ftdi_read_data_submit failed");

		return r;
	}

	~FTDITransferControl()
	{
		// don't throw from dtor
		finishImpl();
	}

private:
	int finishImpl()
	{
		int r = 0;
		if (m_tc)
		{
			r = ftdi_transfer_data_done(m_tc);
			m_tc = nullptr;
		}
		return r;
	}

	FTDIContext & m_context;
	ftdi_transfer_control * m_tc;
};

/**
 * A simple serializer for MPSSE commands.
 *
 * The doc (neither FTDI doc, nor libftdi doc) is particularly clear about
 * the size limit of a MPSSE command buffer. But it turns out any size is
 * OK. libftdi's ftdi_read_data/ftdi_write_data anyway transfers the data
 * in blocks of readbuffer_chunksize. So when larger block is used, liftdi
 * slices it up anyway. And the FTDI chip doesn't seem to mind a command
 * that is split into multiple chunks (so, multiple URBs?).
 *
 * But as the data is anyway sliced and diced, it also doesn't make sense to
 * try to keep it in 1 buffer. Just pick a reasonable size and use a vector
 * of vectors.
 */
class MPSSECommandSerializer
{
public:
	/**
	 * Create an instance of the MPSSE command serializer.
	 *
	 * @param[in] maxBufferSize Maximal size of a single buffer. When more data
	 * needs to be stored, a new buffer is created. There don't seem to be any
	 * real limitation on the size either libftdi or the FTDI chip, so any
	 * reasonable value will do.
	 */
	MPSSECommandSerializer(size_t maxBufferSize = 16*1024):
		m_maxBufferSize(maxBufferSize)
	{}

	// this sets ADBUS pins
	void setDataBitsLowByte(std::bitset<8> outDirections, std::bitset<8> outValues)
	{
		push({
			SET_BITS_LOW,
			static_cast<uint8_t>(outValues.to_ulong()),
			static_cast<uint8_t>(outDirections.to_ulong())
		});
	}

	// this reads ADBUS pins
	void readDataBitsLowByte()
	{
		push(GET_BITS_LOW);
	}

	// this sets ACBUS pins
	void setDataBitsHighByte(std::bitset<8> outDirections, std::bitset<8> outValues)
	{
		push({
			SET_BITS_HIGH,
			static_cast<uint8_t>(outValues.to_ulong()),
			static_cast<uint8_t>(outDirections.to_ulong())
		});
	}

	// this reads ACBUS pins
	void readDataBitsHighByte()
	{
		push(GET_BITS_HIGH);
	}

	void enableClockDivideBy5()
	{
		push(EN_DIV_5);
	}

	void setClockDivisor(uint16_t clockDivisor)
	{
		push({
			TCK_DIVISOR,
			static_cast<uint8_t>(clockDivisor & 0xff),
			static_cast<uint8_t>(clockDivisor >> 8)
		});
	}

	void disableThreePhaseDataClocking()
	{
		push(DIS_3_PHASE);
	}

	void disableAdaptiveClocking()
	{
		push(DIS_ADAPTIVE);
	}

	void setLoopback(bool enabled)
	{
		push(enabled ? LOOPBACK_START : LOOPBACK_END);
	}

	void writeBytes(uint8_t writeCommand /*TODO: better abstraction*/, const uint8_t * data, unsigned size)
	{
		serializeReadWriteCommandWithData(writeCommand, data, size);
	}

	void writeBits(uint8_t writeCommand /*TODO: better abstraction*/, uint8_t bits, unsigned size)
	{
		if (size == 0)
			return; // do nothing

		if (size > 8)
			throw std::runtime_error("writeBits can only write <= 8 bits");

		push({
			writeCommand,
			static_cast<uint8_t>(size - 1),
			bits
		});
	}

	void readBytes(uint8_t readCommand /*TODO: better abstraction*/, unsigned size)
	{
		while (size > 0)
		{
			const unsigned currentSize = std::min(MAX_COMMAND_PAYLOAD, size);
			push({
				readCommand,
				static_cast<uint8_t>((currentSize-1) & 0xff),
				static_cast<uint8_t>((currentSize-1) >> 8)
			});
			size -= currentSize;
		}
	}

	void readWriteBytes(uint8_t readWriteCommand /*TODO: better abstraction*/, const uint8_t * writeData, unsigned size)
	{
		serializeReadWriteCommandWithData(readWriteCommand, writeData, size);
	}

	void execCommand(FTDIContext & context)
	{
		for (auto & buffer : m_buffers)
		{
			if (ftdi_write_data(context, buffer.data(), buffer.size()) < 0)
				throw FTDIError(context, "ftdi_write_data failed");
		}

		m_buffers.clear();
	}

	void execCommandWithRead(FTDIContext & context, size_t size, std::vector<uint8_t> & data)
	{
		// initiate async read
		data.resize(size);

		FTDITransferControl tc(context, ftdi_read_data_submit(context, data.data(), data.size()));
		if (!tc)
		{
			data.clear();
			throw FTDIError(context, "ftdi_read_data_submit failed");
		}

		int r;
		try
		{
			execCommand(context);
			r = tc.finish();
		}
		catch (...)
		{
			data.clear();
			throw;
		}

		data.resize(r);
	}

private:
	void ensureBufferAvailable()
	{
		if (m_buffers.empty() || m_buffers.back().size() == m_maxBufferSize)
		{
			// allocate a new buffer
			m_buffers.emplace_back();
		}
	}

	void push(const std::initializer_list<uint8_t> & bytes)
	{
		const auto end = bytes.end();
		auto it = bytes.begin();

		while (it != end)
		{
			ensureBufferAvailable();
			m_buffers.back().push_back(*it);
			++it;
		}
	}

	void push(uint8_t b)
	{
		ensureBufferAvailable();
		m_buffers.back().push_back(b);
	}

	void push(const uint8_t * data, unsigned size)
	{
		const uint8_t * const end = data + size;
		while (data != end)
		{
			ensureBufferAvailable();
			const size_t bytesToCopy = std::min(m_maxBufferSize - m_buffers.back().size(), (size_t)(end - data));
			std::copy(data, data + bytesToCopy, std::back_inserter(m_buffers.back()));
			data += bytesToCopy;
		}
	}

	void serializeReadWriteCommandWithData(uint8_t command, const uint8_t * data, unsigned size)
	{
		while (size > 0)
		{
			const unsigned currentSize = std::min(MAX_COMMAND_PAYLOAD, size);
			push({
				command,
				static_cast<uint8_t>((currentSize-1) & 0xff),
				static_cast<uint8_t>((currentSize-1) >> 8)
			});
			push(data, currentSize);

			data += currentSize;
			size -= currentSize;
		}
	}

	// A single read/write command can do at most 0x10000 bytes.
	static constexpr unsigned MAX_COMMAND_PAYLOAD = 0x10000;

	size_t m_maxBufferSize;
	std::vector<std::vector<uint8_t>> m_buffers;
};

#endif // RFM69_MPSSE_COMMAND_SERIALIZER_H
