/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "FTDIContext.h"

#include <sstream>

FTDIContext::FTDIContext(const char * device, ftdi_interface iface):
	m_context(ftdi_new(), ftdi_free)
{
	if (!m_context)
		throw std::runtime_error("can't create libftdi context");

	if (ftdi_set_interface(m_context.get(), iface) < 0)
		throw FTDIError(*this, "can't set FTDI interface");

	if (ftdi_usb_open_string(m_context.get(), device) < 0)
	{
		std::stringstream s;
		s << "can't open FTDI device '" << device << '\'';
		throw FTDIError(*this, s.str());
	}

	if (ftdi_usb_reset(m_context.get()) < 0)
		throw FTDIError(*this, "can't reset device");

	if (ftdi_usb_purge_buffers(m_context.get()) < 0)
		throw FTDIError(*this, "can't purge buffers");
}

FTDIContext::~FTDIContext()
{
	ftdi_usb_close(*this);
}

