/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "FTDISPI.h"

#include <stdexcept>

namespace
{
	constexpr uint8_t MPSSE_LOW_BYTE_BIT_TCK = 1 << 0;
	constexpr uint8_t MPSSE_LOW_BYTE_BIT_DO = 1 << 1;
	constexpr uint8_t MPSSE_LOW_BYTE_BIT_CS = 1 << 3;

	/**
	 * MPSSE command used for writing.
	 *
	 * SPI mode 0: write on preceding clock falling edge
	 */
	constexpr uint8_t WRITE_COMMAND = MPSSE_DO_WRITE | MPSSE_WRITE_NEG;

	/**
	 * MPSSE command used for reading.
	 *
	 * SPI mode 0: read on clock raising edge
	 */
	constexpr uint8_t READ_COMMAND = MPSSE_DO_READ;
}

/**
 * Ensures a slave is selected on construction and deselected on destruction
 * unless it's been already selected by the caller. In such case SS is not
 * touched.
 */
class FTDISPI::ScopedSlaveSelector
{
public:
	explicit ScopedSlaveSelector(FTDISPI & spi):
		m_select(!spi.m_selected),
		m_spi(spi)
	{
		if (m_select)
			m_spi.setSS(true);
	}

	~ScopedSlaveSelector()
	{
		if (m_select)
			m_spi.setSS(false);
	}

private:
	const bool m_select;
	FTDISPI & m_spi;
};

FTDISPI::FTDISPI(const char * device, ftdi_interface iface, unsigned clockFrequency):
	m_context(device, iface)
{
	/*
	 * Enable MPSSE and configure for SPI. This is inspired by:
	 * * https://github.com/UweBonnes/libftdispi/blob/master/src/ftdispi.c
	 * * https://www.ftdichip.com/Support/Documents/AppNotes/AN_135_MPSSE_Basics.pdf
	 *
	 * This is intended for communication with RFM69, so it does not have to be generic.
	 * This class uses: cpol = 0, cpha = 0
	 */

	/*
	 * Decrease the time the library is willing to wait for incomplete packet.
	 * This should decrease I/O latency as often the SPI device does not produce
	 * enough data to fill in full packet.
	 */
	if (ftdi_set_latency_timer(m_context, 2) < 0)
		throw FTDIError(m_context, "can't set latency");

	// libftdispi does this... but is it needed? how does flow control work with MPSSE?
	// https://www.ftdichip.com/Support/Documents/AppNotes/AN_135_MPSSE_Basics.pdf says
	// in 4.2 Configure FTDI Port For MPSSE Use: "Configure for RTS/CTS flow control to
	// ensure that the driver will not issue IN requests if the buffer is unable to accept
	// data."
	if (ftdi_setflowctrl(m_context, SIO_RTS_CTS_HS) < 0)
		throw FTDIError(m_context, "can't set flow control to RTS/CTS");

	// AN_135 says we should first do a RESET and then enable MPSSE
	if (ftdi_set_bitmode(m_context, 0, BITMODE_RESET) < 0)
		throw FTDIError(m_context, "can't reset bitmode");

	if (ftdi_set_bitmode(m_context, 0, BITMODE_MPSSE) < 0)
		throw FTDIError(m_context, "can't set bitmode to MPSSE");

	if (ftdi_usb_purge_buffers(m_context) < 0)
		throw FTDIError(m_context, "can't purge buffers");

	checkDevicePresent();

	/*
	 * Bits that need to be configured as output for SPI.
	 * TCK/SK = SCLK
	 * TDI/DO = MOSI
	 * TMS/CS = SS
	 * (TDO/DI is MISO and is configured as input)
	 */
	m_gpioLoDirections = MPSSE_LOW_BYTE_BIT_TCK | MPSSE_LOW_BYTE_BIT_DO | MPSSE_LOW_BYTE_BIT_CS;

	// SS is idle at 1, clock is idle at 0
	m_gpioLoValue = MPSSE_LOW_BYTE_BIT_CS;

	// set the Low bits to set the clock idle value and to set the SS to inactive
	m_serializer.setDataBitsLowByte(m_gpioLoDirections, m_gpioLoValue);

	// set clock
	m_serializer.setClockDivisor(DIV_VALUE(clockFrequency));

	// execute buffered commands
	m_serializer.execCommand(m_context);
}

void FTDISPI::assertSS()
{
	if (m_selected)
		throw std::runtime_error("Slave already selected!");

	setSS(true);
	m_serializer.execCommand(m_context);
	m_selected = true;
}

void FTDISPI::deassertSS()
{
	ensureSelected();

	setSS(false);
	m_serializer.execCommand(m_context);
	m_selected = false;
}

void FTDISPI::writeBytes(const uint8_t * data, unsigned size)
{
	{
		ScopedSlaveSelector s(*this);
		m_serializer.writeBytes(WRITE_COMMAND, data, size);
	}
	m_serializer.execCommand(m_context);
}

void FTDISPI::writeAndReadBytes(const uint8_t * writeData, unsigned writeSize, std::vector<uint8_t> & readData, unsigned readSize)
{
	{
		ScopedSlaveSelector s(*this);
		// a write followed by read (not at the same time)
		m_serializer.writeBytes(WRITE_COMMAND, writeData, writeSize);
		m_serializer.readBytes(READ_COMMAND, readSize);
	}

	m_serializer.execCommandWithRead(m_context, readSize, readData);
}

void FTDISPI::setSS(bool asserted)
{
	// SS is idle at 1, so asserting SS means driving it low
	if (asserted)
	{
		// set MPSSE_LOW_BYTE_BIT_CS to 0
		m_gpioLoValue &= ~MPSSE_LOW_BYTE_BIT_CS;
	}
	else
	{
		// set MPSSE_LOW_BYTE_BIT_CS to 1
		m_gpioLoValue |= MPSSE_LOW_BYTE_BIT_CS;
	}
	m_serializer.setDataBitsLowByte(m_gpioLoDirections, m_gpioLoValue);
}

void FTDISPI::checkDevicePresent()
{
	/*
	 * Do what AN_135 recommends:
	 * Send a deliberately invalid command byte and check we get invalid-command reply.
	 * It's not so obvious what command is invalid command (what if a now-unused byte
	 * gets assigned a valid command in some later revision?), but let's use what AN_135
	 * example uses. This will hopefully stay unused.
	 */
	const uint8_t invalidCommand = 0xab;
	if (ftdi_write_data(m_context, &invalidCommand, sizeof(invalidCommand)) < 0)
		throw FTDIError(m_context, "ftdi_write_data failed");

	/*
	 * I started getting 0 byte reply here on the 1st attempt. If I retry again,
	 * I get the expected 2 bytes. I do not know what triggered this change - the
	 * hardware is still the same. Perhaps some libftdi or libusb change? Or kernel
	 * change?
	 *
	 * Anyway, it seems that retrying helps here. Currently it seems that at most
	 * 1 retry is sufficient and it never happens that I receive only 1 byte in
	 * the first attempt.
	 */
	constexpr size_t MAX_RETRIES = 1;
	uint8_t reply[2];
	int replySize = 0;
	for (size_t i = 0; i < MAX_RETRIES + 1 && replySize == 0; i++)
	{
		replySize = ftdi_read_data(m_context, reply, sizeof(reply));
		if (replySize < 0)
			throw FTDIError(m_context, "ftdi_read_data failed");
	}

	const char ERROR_PREFIX[] = "Unexpected reply when checking device presence: ";
	if (replySize != 2)
	{
		std::stringstream s;
		s << ERROR_PREFIX << "got " << replySize << " bytes but expected 2";
		throw std::runtime_error(s.str());
	}

	if (reply[0] != 0xfa /* invalid command */ || reply[1] != invalidCommand)
	{
		std::stringstream s;
		s << ERROR_PREFIX << "unexpected reply";
		throw std::runtime_error(s.str());
	}
}

void FTDISPI::ensureSelected()
{
	if (!m_selected)
		throw std::runtime_error("Slave not selected!");
}
