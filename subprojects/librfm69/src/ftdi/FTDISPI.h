/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RFM69_FTDI_SPI_H
#define RFM69_FTDI_SPI_H

#include <cstdint>

#include <ftdi.h>

#include "MPSSECommandSerializer.h"
#include "../ISPI.h"

class FTDISPI: public ISPI
{
public:
	FTDISPI(const char * device, ftdi_interface iface, unsigned clockFrequency);

protected:
	virtual void writeBytes(const uint8_t * data, unsigned size) override;
	virtual void writeAndReadBytes(const uint8_t * writeData, unsigned writeSize,
			std::vector<uint8_t> & readData, unsigned readSize) override;

	virtual void assertSS() override;
	virtual void deassertSS() override;

private:
	class ScopedSlaveSelector;

	void checkDevicePresent();
	void ensureSelected();
	void setSS(bool asserted);

	bool m_selected = false;
	FTDIContext m_context;
	MPSSECommandSerializer m_serializer;

	uint8_t m_gpioLoDirections;
	uint8_t m_gpioLoValue;
};

#endif // RFM69_FTDI_SPI_H
