/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RFM69_FTDI_CONTEXT_H
#define RFM69_FTDI_CONTEXT_H

#include <memory>
#include <stdexcept>
#include <string>

#include <ftdi.h>

#include "FTDIError.h"

class FTDIContext
{
public:
	FTDIContext(const char * device, ftdi_interface iface);

	FTDIContext(const FTDIContext &) = delete;
	FTDIContext& operator=(const FTDIContext) = delete;

	~FTDIContext();

	ftdi_context * get()
	{
		return m_context.get();
	}

	ftdi_context * get() const
	{
		return m_context.get();
	}

	operator ftdi_context *()
	{
		return get();
	}

private:
	std::unique_ptr<ftdi_context, decltype(&ftdi_free)> m_context;
};

#endif // RFM69_FTDI_CONTEXT_H
