/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RFM69_ISPI_H
#define RFM69_ISPI_H

#include <cstdint>
#include <vector>

class ISPI
{
public:
	/**
	 * RAII class for asserting SPI slave select line.
	 *
	 * When an instance is created (via ISPI::select()), the SS line
	 * is asserted and when the instance is destroyed, the SS line is
	 * deasserted.
	 *
	 * The SlaveSelect instances are not copyable (but they can be moved).
	 *
	 * @note Use ISPI::select() to get an instance of this class.
	 */
	class SlaveSelect
	{
	private:
		explicit SlaveSelect(ISPI & spi):
			m_spi(spi)
		{
			m_spi.assertSS();
		}

	public:
		~SlaveSelect()
		{
			m_spi.deassertSS();
		}

		/**
		 * Write bytes to SPI.
		 *
		 * Data is sent (as 8 bit bytes, MSB-first) over SPI to the slave.
		 */
		void writeBytes(const uint8_t * data, unsigned size)
		{
			m_spi.writeBytes(data, size);
		}

		/**
		 * Write data, then immediately read.
		 *
		 * This is not full-duplex reading and writing at the same time, instead:
		 *
		 * 1) writeData is sent (as 8 bit bytes, MSB-first) over SPI to the slave
		 * 2) readSize bytes from the slave
		 */
		void writeAndReadBytes(const uint8_t * writeData, unsigned writeSize,
				std::vector<uint8_t> & readData, unsigned readSize)
		{
			m_spi.writeAndReadBytes(writeData, writeSize, readData, readSize);
		}

	private:
		ISPI & m_spi;

		friend class ISPI;
	};

	/**
	 * Activate the slave by asserting its SS line.
	 *
	 * @return An instance of SlaveSelect that can be used to issue reads from
	 * and writes to the slave. When the instance is destroyed, the slave is
	 * deselected.
	 *
	 * @todo Support multiple slaves.
	 */
	SlaveSelect select()
	{
		return SlaveSelect(*this);
	}

	virtual ~ISPI() = default;

protected:
	/**
	 * Write bytes to SPI.
	 *
	 * Data is sent (as 8 bit bytes, MSB-first) over SPI to the slave.
	 *
	 * @note This function must be accessed via SlaveSelect instance.
	 */
	virtual void writeBytes(const uint8_t * data, unsigned size) = 0;

	/**
	 * Write data, then immediately read.
	 *
	 * This is not full-duplex reading and writing at the same time, instead:
	 *
	 * 1) writeData is sent (as 8 bit bytes, MSB-first) over SPI to the slave
	 * 2) readSize bytes from the slave
	 *
	 * @note This function must be accessed via SlaveSelect instance.
	 */
	virtual void writeAndReadBytes(const uint8_t * writeData, unsigned writeSize,
			std::vector<uint8_t> & readData, unsigned readSize) = 0;

	/**
	 * Select the slave. Currently there is only 1 slave supported.
	 * @note This function can be accessed only indirectly via SlaveSelect.
	 *
	 * @todo Support multiple slaves.
	 */
	virtual void assertSS() = 0;

	/**
	 * Deselect the slave.  Currently there is only 1 slave supported.
	 * @note This function can be accessed only indirectly via SlaveSelect.
	 *
	 * @todo Support multiple slaves.
	 */
	virtual void deassertSS() = 0;
};

#endif // RFM69_ISPI_H
