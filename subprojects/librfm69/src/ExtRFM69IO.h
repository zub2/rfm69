/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RFM69_EXTRFM69IO_H
#define RFM69_EXTRFM69IO_H

#include <memory>
#include <string>

#include "rfm69/IRFM69IO.h"

class ExtRFM69IO: public IRFM69IO
{
public:
	ExtRFM69IO(std::string && readCmd, std::string && writeCmd, bool verbose);
	virtual uint8_t readRegister(uint8_t r) override;
	virtual std::vector<uint8_t> readRegisters(uint8_t r, size_t count) override;
	virtual void writeRegister(uint8_t r, uint8_t value) override;
	virtual void writeRegisters(uint8_t r, const uint8_t * values, size_t count) override;

private:
	uint8_t readRegisterImpl(uint8_t r);
	void writeRegisterImpl(uint8_t r, uint8_t value);

	const std::string m_readCmd;
	const std::string m_writeCmd;
	const bool m_verbose;
};

#endif // RFM69_EXTRFM69IO_H
