/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RFM69_SPIRFM69IO_H
#define RFM69_SPIRFM69IO_H

#include <memory>

#include "ISPI.h"
#include "rfm69/IRFM69IO.h"

class SPIRFM69IO: public IRFM69IO
{
public:
	SPIRFM69IO(std::unique_ptr<ISPI> && spi, bool verbose);
	virtual uint8_t readRegister(uint8_t r) override;
	virtual std::vector<uint8_t> readRegisters(uint8_t r, size_t count) override;
	virtual void writeRegister(uint8_t r, uint8_t value) override;
	virtual void writeRegisters(uint8_t r, const uint8_t * values, size_t count) override;

private:
	void writeBytes(const uint8_t * data, unsigned size);
	void writeAndReadBytes(const uint8_t * writeData, unsigned writeSize,
			std::vector<uint8_t> & readData, unsigned readSize);

	/**
	 * Interface used to access SPI.
	 */
	const std::unique_ptr<ISPI> m_spi;

	const bool m_verbose;
};

#endif // RFM69_SPIRFM69IO_H
