/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ExtRFM69IO.h"

#include <optional>
#include <limits>
#include <iostream>
#include <system_error>
#include <cerrno>

#include <stdio.h> // popen is posix, so it's not guaranteed to be pulled in by cstdio

namespace
{
	template<typename F>
	void runShellCommandImpl(const std::string & shellCommand, F && f)
	{
		FILE * file = popen(shellCommand.c_str(), "r");
		if (!file)
			throw std::system_error(errno, std::generic_category(), std::string("can't run: ") + shellCommand);

		f(file);

		// TODO: RAII?
		pclose(file);
	}

	void runShellCommand(const std::string & shellCommand)
	{
		runShellCommandImpl(shellCommand, [](FILE * f){});
	}

	uint8_t runShellCommandWithResult(const std::string & shellCommand)
	{
		std::optional<unsigned> result;
		runShellCommandImpl(shellCommand, [&result](FILE * f){
			unsigned r;
			if (fscanf(f, "%u", &r) == 1)
				result = r;
		});

		if (!result)
			throw std::runtime_error("can't parse output of the command as an unsigned integer");

		if (*result > std::numeric_limits<uint8_t>::max())
			throw std::runtime_error("return value too large to fit in an uint8");

		return static_cast<uint8_t>(*result);
	}

	void advanceRegister(uint8_t & r)
	{
		// RegFifo is not incremented
		if (r != 0)
			r++;
	}
}

ExtRFM69IO::ExtRFM69IO(std::string && readCmd, std::string && writeCmd, bool verbose):
	m_readCmd(std::move(readCmd)),
	m_writeCmd(std::move(writeCmd)),
	m_verbose(verbose)
{}

uint8_t ExtRFM69IO::readRegister(uint8_t r)
{
	if (m_verbose)
	{
		std::cout << "ExtIO: reading register 0x" << std::hex << (unsigned)r << std::endl;
	}

	return readRegisterImpl(r);
}

std::vector<uint8_t> ExtRFM69IO::readRegisters(uint8_t r, size_t count)
{
	if (m_verbose)
	{
		std::cout << "ExtIO: reading registers 0x" << std::hex << (unsigned)r << "..0x" << (r+count - 1) << std::endl;
	}

	std::vector<uint8_t> v;
	v.reserve(count);

	for (size_t i = 0; i < count; i++)
	{
		v.push_back(readRegisterImpl(r));
		advanceRegister(r);
	}
	return v;
}

void ExtRFM69IO::writeRegister(uint8_t r, uint8_t value)
{
	if (m_verbose)
	{
		std::cout << "ExtIO: writing register 0x" << std::hex << (unsigned)r << ": 0x" << (unsigned)value << std::endl;
	}

	writeRegisterImpl(r, value);
}

void ExtRFM69IO::writeRegisters(uint8_t r, const uint8_t * values, size_t count)
{
	if (m_verbose)
	{
		std::cout << "ExtIO: writing registers 0x" << std::hex << (unsigned)r << "..0x" << (r+count - 1) << std::endl;
	}

	for (size_t i = 0; i < count; i++)
	{
		writeRegisterImpl(r, values[i]);
		advanceRegister(r);
	}
}

uint8_t ExtRFM69IO::readRegisterImpl(uint8_t r)
{
	std::string cmd = m_readCmd + " " + std::to_string((unsigned)r);

	if (m_verbose)
		std::cout << "ExtIO: running read cmd: '" << cmd << '\'' << std::endl;

	return runShellCommandWithResult(cmd);
}

void ExtRFM69IO::writeRegisterImpl(uint8_t r, uint8_t value)
{
	std::string cmd = m_readCmd + " " + std::to_string((unsigned)r) + " " + std::to_string((unsigned)value);

	if (m_verbose)
		std::cout << "ExtIO: running write cmd: '" << cmd << '\'' << std::endl;

	runShellCommand(cmd);
}
