/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SPIRFM69IO.h"

#include <iostream>

SPIRFM69IO::SPIRFM69IO(std::unique_ptr<ISPI> && spi, bool verbose):
	m_spi(std::move(spi)),
	m_verbose(verbose)
{}

uint8_t SPIRFM69IO::readRegister(uint8_t r)
{
	// See RFM69 spec, 5.2.1. SPI Interface: send 1 byte: the register
	// number and read 1 byte - the register value
	// The register number has only 7 bits, the MSB is 0 for reading.

	std::vector<uint8_t> reply;
	writeAndReadBytes(&r, 1, reply, 1);

	if (reply.size() != 1)
		throw std::runtime_error("SPI communication error");

	if (m_verbose)
	{
		std::cout << __func__ << "[" << std::hex << std::showbase << static_cast<unsigned>(r)
				<< "] = " << static_cast<unsigned>(reply.front()) << std::dec << std::endl;
	}

	return reply.front();
}

std::vector<uint8_t> SPIRFM69IO::readRegisters(uint8_t r, size_t count)
{
	// See RFM69 spec, 5.2.1. SPI Interface: send 1 byte: the register
	// number and read count bytes - value of registers r, r+1, ..., r+count
	// The register number has only 7 bits, the MSB is 0 for reading.

	if (count == 0)
		return {}; // come on

	std::vector<uint8_t> reply;
	writeAndReadBytes(&r, 1, reply, count);

	if (reply.size() != count)
		throw std::runtime_error("SPI communication error");

	if (m_verbose)
	{
		std::cout << __func__ << "[" << std::hex << std::showbase << static_cast<unsigned>(r)
				<< ".." << (unsigned)(r + count - 1) << "] = {";

		for (size_t i = 0; i < reply.size(); i++)
		{
			if (i != 0)
				std::cout << ", ";

			std::cout << (unsigned)reply[i];
		}

		std::cout << std::dec << "}" << std::endl;
	}

	return reply;
}

void SPIRFM69IO::writeRegister(uint8_t r, uint8_t value)
{
	// Similar to read, but the request MSB must be 1 and the desired value
	// is sent as a second byte of the request.

	const uint8_t request[] =
	{
			static_cast<uint8_t>(r | 0b10000000),
			value
	};

	if (m_verbose)
	{
		std::cout << __func__ << "[" << std::hex << std::showbase << static_cast<unsigned>(r)
				<< "] = " << static_cast<unsigned>(value) << std::dec << std::endl;
	}

	writeBytes(request, sizeof(request));
}

void SPIRFM69IO::writeRegisters(uint8_t r, const uint8_t * values, size_t count)
{
	// Similar to read, but the request MSB must be 1 and the desired value
	// is sent as a second byte of the request. When more than 1 byte
	// is sent, they are either stored in subsequent registers, or, for
	// FIFO, they are pushed into the FIFO register.

	std::vector<uint8_t> request;
	request.reserve(count + 1);
	request.push_back(static_cast<uint8_t>(r | 0b10000000));
	std::copy(values, values + count, std::back_inserter(request));

	if (m_verbose)
	{
		std::cout << __func__ << "[" << std::hex << std::showbase << static_cast<unsigned>(r)
				<< "] = ";

		for (size_t i = 0; i < count; i++)
		{
			if (i != 0)
				std::cout << ',';

			std::cout << static_cast<unsigned>(values[i]);
		}
		std::cout << std::dec << std::endl;
	}

	writeBytes(request.data(), request.size());
}

void SPIRFM69IO::writeBytes(const uint8_t * data, unsigned size)
{
	m_spi->select().writeBytes(data, size);
}

void SPIRFM69IO::writeAndReadBytes(const uint8_t * writeData, unsigned writeSize,
		std::vector<uint8_t> & readData, unsigned readSize)
{
	m_spi->select().writeAndReadBytes(writeData, writeSize, readData, readSize);
}
