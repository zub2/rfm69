/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "rfm69/RFM69.h"

#include <cstdint>
#include <cmath>
#include <vector>
#include <stdexcept>
#include <iterator>
#include <iostream>
#include <iomanip>

#include "SPIRFM69IO.h"
#include "ftdi/FTDISPI.h"

#include "ExtRFM69IO.h"

namespace
{
	void verifyRegisterNumber(uint8_t r)
	{
		if (r & 0b10000000)
			throw std::runtime_error("register number out of range");
	}
}

RFM69::RFM69(const char * device, ftdi_interface iface, unsigned clockFrequency, bool highPower, bool verbose):
	m_io(std::make_unique<SPIRFM69IO>(std::make_unique<FTDISPI>(device, iface, clockFrequency), verbose)),
	m_highPower(highPower)
{}

RFM69::RFM69(std::string && readCommand, std::string && writeCommand, bool highPower, bool verbose):
	m_io(std::make_unique<ExtRFM69IO>(std::move(readCommand), std::move(writeCommand), verbose)),
	m_highPower(highPower)
{}

// force the dtor to be generated here (so that ISPI can be only forward-declared in the header)
RFM69::~RFM69()
{}

std::pair<unsigned, unsigned> RFM69::getVersion()
{
	const uint8_t v = readRegister(RegVersion);
	return std::make_pair(v >> 4, v & 0xf);
}

void RFM69::setMode(Mode mode)
{
	uint8_t modeValue;
	switch (mode)
	{
	case Mode::Sleep:
		modeValue = 0b000;
		break;

	case Mode::StandBy:
		modeValue = 0b001;
		break;

	case Mode::Transmit:
		modeValue = 0b011;
		break;
	}

	const uint8_t v = static_cast<uint8_t>(modeValue << 2);

	writeRegister(RegOpMode, v);

	/* Wait until mode switch is finished.
	 * This is indicated by the MSB being set in RegIrqFlags1.
	 */
	uint8_t flags = readRegister(RegIrqFlags1);
	while ((flags & 0b10000000) == 0)
		flags = readRegister(RegIrqFlags1);
}

void RFM69::setBitRate(uint16_t bitrate)
{
	const uint8_t values[2] =
	{
		static_cast<uint8_t>((bitrate >> 8) & 0xff), // MSB
		static_cast<uint8_t>(bitrate & 0xff) // LSB
	};
	writeRegisters(RegBitrateMsb, values, std::size(values));
}

void RFM69::setFrequency(uint32_t frequency)
{
	const long frequencyCoeff = std::lround(frequency / FSTEP);

	if (frequencyCoeff > UINT32_C(0xffffff))
		throw std::runtime_error("Frequency out of range (0..0xffffff).");

	const uint8_t values[3] =
	{
		static_cast<uint8_t>((frequencyCoeff >> 16) & 0xff), // MSB
		static_cast<uint8_t>((frequencyCoeff >> 8) & 0xff), // Middle
		static_cast<uint8_t>(frequencyCoeff & 0xff) // LSB
	};
	writeRegisters(RegFrfMsb, values, std::size(values));
}

void RFM69::disableSyncBytes()
{
	setSyncConfig(0);
}

void RFM69::setSyncBytes(const std::vector<uint8_t> & syncBytes)
{
	setSyncConfig(syncBytes.size());
	writeRegisters(RegSyncValue1, syncBytes.data(), syncBytes.size());
}

void RFM69::setSyncConfig(unsigned syncWordSize)
{
	uint8_t syncConfig = 0b0011000; // default value from spec

	if (syncWordSize > 0b111 + 1)
		throw std::runtime_error("Sync word too long. Maximum is 8.");

	if (syncWordSize > 0)
		syncConfig |= (1 << 7) | ((syncWordSize - 1) << 3);

	writeRegister(RegSyncConfig, syncConfig);
}

void RFM69::setPacketConfig(PacketFormat packetFormat, DCFree dcFree, bool crc,
		bool autoClearOnCrcError, AddressFiltering addressFiltering)
{
	uint8_t vPacketFormat;
	switch (packetFormat)
	{
	case PacketFormat::FixedLength:
		vPacketFormat = 0;
		break;
	case PacketFormat::VariableLength:
		vPacketFormat = 1;
		break;
	}

	uint8_t vDcFree;
	switch (dcFree)
	{
	case DCFree::None:
		vDcFree = 0b00;
		break;
	case DCFree::Manechester:
		vDcFree = 0b01;
		break;
	case DCFree::Whitening:
		vDcFree = 0b10;
		break;
	}

	const uint8_t vCrcOn = crc;

	const uint8_t vCrcAutoClearOff = !autoClearOnCrcError;

	uint8_t vAddressFiltering;
	switch (addressFiltering)
	{
	case AddressFiltering::None:
		vAddressFiltering = 0b00;
		break;
	case AddressFiltering::MatchNodeAddr:
		vAddressFiltering = 0b010;
		break;
	case AddressFiltering::MatchNodeAddrOfBroadcast:
		vAddressFiltering = 0b10;
		break;
	}

	const uint8_t v = static_cast<uint8_t>(
		(vPacketFormat << 7) |
		(vDcFree << 5) |
		(vCrcOn << 4) |
		(vCrcAutoClearOff << 3) |
		(vAddressFiltering << 1)
	);
	writeRegister(RegPacketConfig1, v);
}

void RFM69::setPacketLength(uint8_t length)
{
	writeRegister(RegPayloadLength, length);
}

void RFM69::setPacketOOKMode(OOKModulationShaping shaping)
{
	uint8_t shapingValue;
	switch (shaping)
	{
	case OOKModulationShaping::None:
		shapingValue = 0b00;
		break;
	case OOKModulationShaping::BR:
		shapingValue = 0b01;
		break;
	case OOKModulationShaping::DoubleBR:
		shapingValue = 0b11;
		break;
	}

	//                .. - packet mode
	//                  .. - OOK
	//                     .. - shaping
	uint8_t value = 0b0001000 | shapingValue;
	writeRegister(RegDataModul, value);
}

void RFM69::disableClkOut()
{
	// CLKOUT is enabled by default, but we don't need it and manual recommends
	// turning it off
	const uint8_t oldValue = readRegister(RegDioMapping2);
	const uint8_t newValue = oldValue | 0b111; // set three lowest bits to 1 to disable CLKOUT

	if (newValue != oldValue)
		writeRegister(RegDioMapping2, newValue);
	// else: already disabled
}

void RFM69::setPreambleSize(uint16_t preambleSize)
{
	const uint8_t values[2] =
	{
		static_cast<uint8_t>((preambleSize >> 8) & 0xff), // MSB
		static_cast<uint8_t>(preambleSize & 0xff) // LSB
	};
	writeRegisters(RegPreambleMsb, values, std::size(values));
}

void RFM69::setFifoTresh(TxStartCondition txStartCondition, uint8_t fifoThreshold)
{
	if (fifoThreshold > 0b1111111)
		throw std::runtime_error("FIFO threshold value exceeds bounds");

	uint8_t vTxStartCondition;
	switch (txStartCondition)
	{
	case TxStartCondition::FifoLevel:
		vTxStartCondition = 0;
		break;
	case TxStartCondition::FifoNotEmpty:
		vTxStartCondition = 1;
		break;
	}

	const uint8_t v = static_cast<uint8_t>(
		(vTxStartCondition << 7) |
		fifoThreshold
	);
	writeRegister(RegFifoThresh, v);
}

void RFM69::setPowerLevel(int level)
{
	constexpr uint8_t Pa0On = 0b10000000;
	constexpr uint8_t Pa1On = 0b01000000;
	constexpr uint8_t Pa2On = 0b00100000;

	// TODO: set RegOcp?

	/*
	 * This is based on the excellent work by André Hessling
	 * https://andrehessling.de/2015/02/07/figuring-out-the-power-level-settings-of-hoperfs-rfm69-hwhcw-modules/
	 * and https://github.com/ahessling/RFM69-STM32
	 */
	if (m_highPower)
	{
		// PA0 is not available, but PA1 and PA2 are
		if (level < -2 || level > 20)
			throw std::runtime_error("Requested power level out of range (-2..+20 dBm for the H devices).");

		if (level <= 13)
		{
			// disable high-power setting
			setHighPowerMode(false);

			// use PA1 only
			writeRegister(RegPaLevel, Pa1On | (level + 18));
		}
		else if (level <= 17)
		{
			// disable high-power setting
			setHighPowerMode(false);

			// use PA1 + PA2
			writeRegister(RegPaLevel, Pa1On | Pa2On | (level + 14));
		}
		else
		{
			// enable high-power setting
			setHighPowerMode(true);

			// use PA1 + PA2
			writeRegister(RegPaLevel, Pa1On | Pa2On | (level + 11));
		}
	}
	else
	{
		// only PA0 is available
		if (level < -18 || level > 13)
			throw std::runtime_error("Requested power level out of range (-18..+13 dBm for the non-H devices).");

		// use PA0
		writeRegister(RegPaLevel, Pa0On | (level + 18));
	}
}

uint8_t RFM69::readRegister(uint8_t r)
{
	// See RFM69 spec, 5.2.1. SPI Interface: send 1 byte: the register
	// number and read 1 byte - the register value
	// The register number has only 7 bits, the MSB is 0 for reading.

	verifyRegisterNumber(r);

	return m_io->readRegister(r);
}

std::vector<uint8_t> RFM69::readRegisters(uint8_t r, size_t count)
{
	// See RFM69 spec, 5.2.1. SPI Interface: send 1 byte: the register
	// number and read count bytes - value of registers r, r+1, ..., r+count
	// The register number has only 7 bits, the MSB is 0 for reading.

	verifyRegisterNumber(r);

	return m_io->readRegisters(r, count);
}

void RFM69::writeRegister(uint8_t r, uint8_t value)
{
	// Similar to read, but the request MSB must be 1 and the desired value
	// is sent as a second byte of the request.

	verifyRegisterNumber(r);

	m_io->writeRegister(r, value);
}

void RFM69::writeRegisters(uint8_t r, const uint8_t * values, size_t count)
{
	// Similar to read, but the request MSB must be 1 and the desired value
	// is sent as a second byte of the request. When more than 1 byte
	// is sent, they are either stored in subsequent registers, or, for
	// FIFO, they are pushed into the FIFO register.

	verifyRegisterNumber(r);

	m_io->writeRegisters(r, values, count);
}

void RFM69::waitForIrqFlags2(uint8_t mask, uint8_t value)
{
	uint8_t irqFlags2;
	do
	{
		irqFlags2 = readRegister(RegIrqFlags2);
	} while ((irqFlags2 & mask) != value);
}

void RFM69::setHighPowerMode(bool enable)
{
	if (!m_highPower)
		throw std::runtime_error("can't enable high power output on a non-high power device!");

	writeRegister(RegTestPa1, enable ? 0x5D : 0x55);
	writeRegister(RegTestPa2, enable ? 0x7C : 0x70);
}
