/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "rfm69/RFM69OOKTransmitter.h"

#include <cmath>
#include <stdexcept>
#include <iostream>

namespace
{
	constexpr size_t FIFO_THRESHOLD = RFM69::FIFO_SIZE / 2;
}

RFM69OOKTransmitter::RFM69OOKTransmitter(RFM69 & rfm69, unsigned frequency, std::chrono::microseconds bitDuration,
		const std::optional<uint8_t> & payloadLength, uint16_t preambleSize, const std::vector<uint8_t> & syncBytes):
	m_rfm69(rfm69)
{
	m_rfm69.setMode(RFM69::Mode::StandBy);
	m_rfm69.disableClkOut();

	if (bitDuration.count() <= 0)
		throw std::runtime_error("Bit duration out of range.");

	const float bitRate = (bitDuration.count() / 1e6f) * RFM69::FXOSC;
	if (bitRate > 0xffff)
		throw std::runtime_error("Bit rate out of range.");

	std::cout << "bitRate=" << bitRate << std::endl;
	m_rfm69.setBitRate(static_cast<uint16_t>(std::lround(bitRate)));

	std::cout << "frequency=" << frequency << std::endl;
	m_rfm69.setFrequency(frequency);
	m_rfm69.setPacketOOKMode(RFM69::OOKModulationShaping::None);

	rfm69.setPreambleSize(preambleSize);
	rfm69.setSyncBytes(syncBytes);

	m_rfm69.setPacketConfig(
		payloadLength ? RFM69::PacketFormat::FixedLength : RFM69::PacketFormat::VariableLength,
		RFM69::DCFree::None,
		false,
		true,
		RFM69::AddressFiltering::None
	);
	if (payloadLength)
		m_rfm69.setPacketLength(*payloadLength);

	m_rfm69.setFifoTresh(RFM69::TxStartCondition::FifoNotEmpty, FIFO_THRESHOLD);

	// TODO: configure PA
}

void RFM69OOKTransmitter::broadcast(const uint8_t * data, size_t size)
{
	const uint8_t * end = data + size;

	if (size > RFM69::FIFO_SIZE)
	{
		std::cout << "Warning: sending " << size << " bytes. This does not fit in RFM69 FIFO ("
			<< RFM69::FIFO_SIZE << "B). There may be gap(s) as the FIFO is re-filled." << std::endl;
	}

	// fill the FIFO before starting transmission
	size_t fillBytes = std::min(RFM69::FIFO_SIZE, size);
	m_rfm69.writeRegisters(RFM69::RegFifo, data, fillBytes);
	data += fillBytes;

	// start transmitting
	m_rfm69.setMode(RFM69::Mode::Transmit);

	// while more data to be send...
	while (data != end)
	{
		// wait for space in FIFO, specifically wait for FifoLevel bit in
		// RegIrqFlags2 is cleared; this means used fifo size is < fifo threshold
		m_rfm69.waitForIrqFlags2(0b100000, 0);

		// fill FIFO
		// we know the FIFO has at least FIFO_THRESHOLD bytes free now
		fillBytes = std::min(FIFO_THRESHOLD, static_cast<size_t>(end - data));
		m_rfm69.writeRegisters(RFM69::RegFifo, data, fillBytes);
		data += fillBytes;
	}

	// wait for transmission end
	m_rfm69.waitForIrqFlags2(0b1000, 0b1000);

	// go back to standby
	m_rfm69.setMode(RFM69::Mode::StandBy);
}
