/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "RFM69RegisterDecoders.h"

#include <iostream>
#include <algorithm>

#include "rfm69/RFM69.h"
#include "Utils.h"

namespace
{
	using std::cout;
	using std::endl;

	using EnumValueDescription = std::pair<uint8_t, const char *>;

	void printEnumImpl(const EnumValueDescription * begin, size_t size, unsigned numBits, uint8_t value)
	{
		const EnumValueDescription * end = begin + size;

		const EnumValueDescription * p = std::find_if(begin, end,
			[value](const EnumValueDescription &ed)
			{
				return value == ed.first;
			});

		if (p != end)
			cout << p->second;
		else
			cout << "RESERVED";

		cout << " (";
		if (numBits > 1)
			cout << toBinary(value, numBits);
		else
			cout << (value & 1);
		cout << ')';
	}

	template<size_t N>
	void printEnum(const EnumValueDescription (&descriptions)[N], unsigned numBits, uint8_t value)
	{
		printEnumImpl(descriptions, N, numBits, value);
	}

	constexpr uint8_t OP_MODE_SLEEP = 0b000;
	constexpr uint8_t OP_MODE_STDBY = 0b001;
	constexpr uint8_t OP_MODE_FS = 0b010;
	constexpr uint8_t OP_MODE_TX = 0b011;
	constexpr uint8_t OP_MODE_RX = 0b100;

	void parseRegOpMode(uint8_t v, bool & sequencerOff, bool & listenOn, bool &listenAbort, uint8_t & mode)
	{
		sequencerOff = v >> 7;
		listenOn = (v >> 6) & 1;
		listenAbort = (v >> 5) & 1;
		mode = (v >> 2) & 0b111;
	}

	const EnumValueDescription EnumOpModeSleep[] =
	{
		{ OP_MODE_SLEEP, "SLEEP" },
		{ OP_MODE_STDBY, "STDBY" },
		{ OP_MODE_FS, "FS" },
		{ OP_MODE_TX, "TX" },
		{ OP_MODE_RX, "RX" }
	};

	void decodeRegOpMode(uint8_t v)
	{
		bool sequencerOff, listenOn, listenAbort;
		uint8_t mode;
		parseRegOpMode(v, sequencerOff, listenOn, listenAbort, mode);

		cout << "SequencerOff: " << sequencerOff << "\n"
				"ListenOn: " << listenOn << "\n"
				"ListenAbort: " << listenAbort << "\n"
				"Mode: ";
		printEnum(EnumOpModeSleep, 3, mode);
		cout << endl;
	}

	void parseRegDataModul(uint8_t v, uint8_t & dataMode, uint8_t & modulationType, uint8_t & modulationShaping)
	{
		dataMode = (v >> 5) & 0b11;
		modulationType = (v >> 3) & 0b11;
		modulationShaping = v & 0b11;
	}

	constexpr uint8_t DATA_MODE_PACKET = 0b00;
	constexpr uint8_t DATA_MODE_CONTINUOUS_WITH_BISTSYNC = 0b10;
	constexpr uint8_t DATA_MODE_CONTINUOUS_WITHOUT_BITSYNC = 0b11;

	const EnumValueDescription EnumDataMode[] =
	{
		{ DATA_MODE_PACKET, "Packet mode" },
		{ DATA_MODE_CONTINUOUS_WITH_BISTSYNC, "Continuous mode with bit synchronizer" },
		{ DATA_MODE_CONTINUOUS_WITHOUT_BITSYNC, "Continuous mode without bit synchronizer" }
	};

	constexpr uint8_t MODULATION_TYPE_FSK = 0b00;
	constexpr uint8_t MODULATION_TYPE_OOK = 0b01;

	const EnumValueDescription EnumModulationType[] =
	{
		{ MODULATION_TYPE_FSK, "FSK" },
		{ MODULATION_TYPE_OOK, "OOK" }
	};

	constexpr uint8_t MODULATION_SHAPING_NONE = 0b00;
	constexpr uint8_t MODULATION_SHAPING_BT10_BR = 0b01;
	constexpr uint8_t MODULATION_SHAPING_BT05_2BR = 0b10;
	constexpr uint8_t MODULATION_SHAPING_BT03 = 0b11;

	const EnumValueDescription EnumModulationShapingFSK[] =
	{
		{ MODULATION_SHAPING_NONE, "no shaping" },
		{ MODULATION_SHAPING_BT10_BR, "Gaussian filter, BT = 1.0" },
		{ MODULATION_SHAPING_BT05_2BR, "Gaussian filter, BT = 0.5" },
		{ MODULATION_SHAPING_BT03, "Gaussian filter, BT = 0.3" }
	};

	const EnumValueDescription EnumModulationShapingOOK[] =
	{
		{ MODULATION_SHAPING_NONE, "no shaping" },
		{ MODULATION_SHAPING_BT10_BR, "filtering with fcutoff = BR" },
		{ MODULATION_SHAPING_BT05_2BR, "filtering with fcutoff = 2*BR" }
	};

	void decodeRegDataModul(uint8_t v)
	{
		uint8_t dataMode, modulationType, modulationShaping;
		parseRegDataModul(v, dataMode, modulationType, modulationShaping);

		cout << "DataMode: ";
		printEnum(EnumDataMode, 2, dataMode);
		cout << "\nModulationType: ";
		printEnum(EnumModulationType, 2, modulationType);
		cout << "\nModulationShaping: ";
		if (modulationType == MODULATION_TYPE_FSK)
			printEnum(EnumModulationShapingFSK, 2, modulationShaping);
		else if (modulationType == MODULATION_TYPE_OOK)
			printEnum(EnumModulationShapingOOK, 2, modulationShaping);
		else
			cout << toBinary(modulationShaping, 2);

		cout << endl;
	}

	void parseRegOsc1(uint8_t v, bool & rcCalStart, bool & rcCalDone)
	{
		rcCalStart = (v >> 7) & 1;
		rcCalDone = (v >> 6) & 1;
	}

	void decodeRegOsc1(uint8_t v)
	{
		bool rcCalStart, rcCalDone;
		parseRegOsc1(v, rcCalStart, rcCalDone);

		cout << "RcCalStart: " << rcCalStart << "\n"
				"RcCalDone: " << rcCalDone << endl;
	}

	void parseRegAfcCtrl(uint8_t v, bool & afcLowBetaOn)
	{
		afcLowBetaOn = (v >> 5) & 1;
	}

	const EnumValueDescription EnumAfcLowBetaOn[] =
	{
		{ false, "Standard AFC routine" },
		{ true, "Improved AFC routine" }
	};

	void decodeRegAfcCtrl(uint8_t v)
	{
		bool afcLowBetaOn;
		parseRegAfcCtrl(v, afcLowBetaOn);
		cout << "AfcLowBetaOn: ";
		printEnum(EnumAfcLowBetaOn, 1, afcLowBetaOn);
		cout<< endl;
	}

	void parseRegVersion(uint8_t v, uint8_t & revision, uint8_t & metalMaskRevision)
	{
		revision = v >> 4;
		metalMaskRevision = v & 0xf;
	}

	void decodeRegVersion(uint8_t v)
	{
		uint8_t revision, metalMaskRevision;
		parseRegVersion(v, revision, metalMaskRevision);

		cout << "revision: " << static_cast<unsigned>(revision) << "\n"
				"metalMaskRevision: " << static_cast<unsigned>(metalMaskRevision) << std::endl;
	}

	void parseRegPaLevel(uint8_t v, bool & pa0On, bool & pa1On, bool & pa2On, uint8_t & outputPower)
	{
		pa0On = v >> 7;
		pa1On = (v >> 6) & 1;
		pa2On = (v >> 5) & 1;
		outputPower = v & 0b11111;
	}

	void decodeRegPaLevel(uint8_t v)
	{
		 bool pa0On, pa1On, pa2On;
		 uint8_t outputPower;
		 parseRegPaLevel(v, pa0On, pa1On, pa2On, outputPower);

		 const int pout = -18 + outputPower;

		 cout << "Pa0On: " << pa0On << "\n"
				 "Pa1On: " << pa1On << "\n"
				 "Pa2On: " << pa2On << "\n"
				 "OutputPower: " << (unsigned)outputPower << " (" << toHex(outputPower, 2) << ") "
				 	 "[" << pout << " dBm]" << std::endl;
	}

	void parseRegPaRamp(uint8_t v, uint8_t & paRamp)
	{
		paRamp = v & 0b1111;
	}

	const EnumValueDescription EnumPaRamp[] =
	{
		{ 0b0000, "3.4 ms" },
		{ 0b0001, "2 ms" },
		{ 0b0010, "1 ms" },
		{ 0b0011, "500 µs" },
		{ 0b0100, "250 µs" },
		{ 0b0101, "125 µs" },
		{ 0b0110, "100 µs" },
		{ 0b0111, "62 µs" },
		{ 0b1000, "50 µs" },
		{ 0b1001, "40 µs" },
		{ 0b1010, "31 µs" },
		{ 0b1011, "25 µs" },
		{ 0b1100, "20 µs" },
		{ 0b1101, "15 µs" },
		{ 0b1110, "12 µs" },
		{ 0b1111, "10 µs" }
	};

	void decodeRegPaRamp(uint8_t v)
	{
		uint8_t paRamp;
		parseRegPaRamp(v, paRamp);

		cout << "PaRamp: ";
		printEnum(EnumPaRamp, 4, paRamp);
		cout << endl;
	}

	void parseRegOcp(uint8_t v, bool & ocpOn, uint8_t & ocpTrim)
	{
		ocpOn = (v >> 4) & 1;
		ocpTrim = v & 0b1111;
	}

	void decodeRegOcp(uint8_t v)
	{
		bool ocpOn;
		uint8_t ocpTrim;
		parseRegOcp(v, ocpOn, ocpTrim);

		const unsigned imax = 45 + 5 * ocpTrim;

		cout << "OcpOn: " << ocpOn << "\n"
				"OcpTrim: " << (unsigned)ocpTrim << "(" << toHex(ocpTrim, 1) << ", " << toBinary(ocpTrim, 4) << ") "
					"[" << imax << " mA]" << std::endl;
	}

	void parseRegDioMapping1(uint8_t v, uint8_t & dio0Mapping, uint8_t & dio1Mapping, uint8_t & dio2Mapping, uint8_t & dio3Mapping)
	{
		dio0Mapping = (v >> 6) & 0b11;
		dio1Mapping = (v >> 4) & 0b11;
		dio2Mapping = (v >> 2) & 0b11;
		dio3Mapping = v & 0b11;
	}

	void decodeRegDioMapping1(uint8_t v)
	{
		uint8_t dio0Mapping, dio1Mapping, dio2Mapping, dio3Mapping;
		parseRegDioMapping1(v, dio0Mapping, dio1Mapping, dio2Mapping, dio3Mapping);

		cout << "Dio0Mapping: " << (unsigned)dio0Mapping << " (" << toBinary(dio0Mapping, 2) << ")\n"
				"Dio1Mapping: " << (unsigned)dio1Mapping << " (" << toBinary(dio1Mapping, 2) << ")\n"
				"Dio2Mapping: " << (unsigned)dio2Mapping << " (" << toBinary(dio2Mapping, 2) << ")\n"
				"Dio3Mapping: " << (unsigned)dio3Mapping << " (" << toBinary(dio3Mapping, 2) << ")\n"
				<< endl;
	}

	void parseRegDioMapping2(uint8_t v, uint8_t & dio5Mapping, uint8_t & clkOut)
	{
		dio5Mapping = (v >> 4) & 0b11;
		clkOut = v & 0b111;
	}

	const EnumValueDescription EnumClkOut[] =
	{
		{ 0b000, "FXOSC" },
		{ 0b001, "FXOSC/2" },
		{ 0b010, "FXOSC/4" },
		{ 0b011, "FXOSC/8" },
		{ 0b100, "FXOSC/16" },
		{ 0b101, "FXOSC/32" },
		{ 0b110, "RC" },
		{ 0b111, "OFF" }
	};

	void decodeRegDioMapping2(uint8_t v)
	{
		uint8_t dio5Mapping;
		uint8_t clkOut;
		parseRegDioMapping2(v, dio5Mapping, clkOut);

		cout << "Dio5Mapping: " << (unsigned)dio5Mapping << " (" << toBinary(dio5Mapping, 2) << ")\n"
				"ClkOut: ";
		printEnum(EnumClkOut, 3, clkOut);
		cout << endl;
	}

	void parseRegIrqFlags1(uint8_t v, bool & modeReady, bool & rxReady, bool & txReady,
			bool & pllLock, bool & rssi, bool & timeout, bool & autoMode, bool & syncAddressMatch)
	{
		modeReady = (v >> 7) & 1;
		rxReady = (v >> 6) & 1;
		txReady = (v >> 5) & 1;
		pllLock = (v >> 4) & 1;
		rssi = (v >> 3) & 1;
		timeout = (v >> 2) & 1;
		autoMode = (v >> 1) & 1;
		syncAddressMatch = v & 1;
	}

	void decodeRegIrqFlags1(uint8_t v)
	{
		bool modeReady, rxReady, txReady, pllLock, rssi, timeout, autoMode, syncAddressMatch;
		parseRegIrqFlags1(v, modeReady, rxReady, txReady, pllLock, rssi, timeout, autoMode, syncAddressMatch);

		cout << "ModeReady: " << modeReady << "\n"
				"RxReady: " << rxReady << "\n"
				"TxReady: " << txReady << "\n"
				"PllLock: " << pllLock << "\n"
				"Rssi: " << rssi << "\n"
				"Timeout: " << timeout << "\n"
				"AutoMode: " << autoMode << "\n"
				"SyncAddressMatch: " << syncAddressMatch << std::endl;
	}

	void parseRegIrqFlags2(uint8_t v, bool & fifoFull, bool & fifoNotEmpty, bool & fifoLevel,
			bool & fifoOverrun, bool & packetSent, bool & payloadReady, bool & crcOk)
	{
		fifoFull = (v >> 7) & 1;
		fifoNotEmpty = (v >> 6) & 1;
		fifoLevel = (v >> 5) & 1;
		fifoOverrun = (v >> 4) & 1;
		packetSent = (v >> 3) & 1;
		payloadReady = (v >> 2) & 1;
		crcOk = (v >> 1) & 1;
	}

	void decodeRegIrqFlags2(uint8_t v)
	{
		bool fifoFull, fifoNotEmpty, fifoLevel, fifoOverrun, packetSent, payloadReady, crcOk;
		parseRegIrqFlags2(v, fifoFull, fifoNotEmpty, fifoLevel, fifoOverrun, packetSent, payloadReady, crcOk);

		cout << "FifoFull: " << fifoFull << "\n"
				"FifoNotEmpty " << fifoNotEmpty << "\n"
				"FifoLevel: " << fifoLevel << "\n"
				"FifoOverrun: " << fifoOverrun << "\n"
				"PacketSent: " << packetSent << "\n"
				"PayloadReady: " << payloadReady << "\n"
				"CrcOk: " << crcOk << endl;
	}

	void decodeRegRssiThresh(uint8_t v)
	{
		const float rssiThreshold = v/-2.0f;
		cout << "RssiThreshold: " << (unsigned)v << " (" << toHex(v, 2) << ") ["
				<< rssiThreshold << " dBm]" << endl;
	}

	void decodeRegRxTimeout1(uint8_t v)
	{
		cout << "TimeoutRxStart: " << (unsigned)v << " (" << toHex(v, 2) << ") [";

		if (v == 0)
			cout << "disabled";
		else
		{
			const int timeoutRxStart = 16 * v;
			cout << timeoutRxStart << "*Tbit";
		}
		cout << ']' << endl;
	}

	void decodeRegRxTimeout2(uint8_t v)
	{
		cout << "TimeoutRssiThresh: " << (unsigned)v << " (" << toHex(v, 2) << ") [";

		if (v == 0)
			cout << "disabled";
		else
		{
			const int timeoutRssiThresh = 16 * v;
			cout << timeoutRssiThresh << "*Tbit";
		}
		cout << ']' << endl;
	}

	void parseRegSyncConfig(uint8_t v, bool & syncOn, bool & fifoFillCondition, uint8_t & syncSize, uint8_t & syncTol)
	{
		syncOn = v >> 7;
		fifoFillCondition = (v >> 6) & 1;
		syncSize = (v >> 3) & 0b111;
		syncTol = v & 0b111;
	}

	const EnumValueDescription EnumFifoFillConditon[] =
	{
		{ false, "if SyncAddress interrupt occurs" },
		{ true, "as long as FifoFillCondition is set" }
	};

	void decodeRegSyncConfig(uint8_t v)
	{
		bool syncOn, fifoFillCondition;
		uint8_t syncSize, syncTol;
		parseRegSyncConfig(v, syncOn, fifoFillCondition, syncSize, syncTol);

		cout << "SyncOn: " << syncOn << "\n"
				"FifoFillCondition: ";
		printEnum(EnumFifoFillConditon, 1, fifoFillCondition);
		cout << "\nSyncSize: " << (unsigned)syncSize << " (" << toBinary(syncSize, 3) << ") [" <<
				((unsigned)syncSize + 1) << "B]\n"
				"SyncTol: " << (unsigned)syncTol << " (" << toBinary(syncTol, 3) << ')' << endl;
	}

	void parseRegPacketConfig1(uint8_t v, bool & packetFormat, uint8_t & dcFree, bool & crcOn, bool & crcAutoClearOff, uint8_t & addressFiltering)
	{
		packetFormat = v >> 7;
		dcFree = (v >> 5) & 0b11;
		crcOn = (v >> 4) & 1;
		crcAutoClearOff = (v >> 3) & 1;
		addressFiltering = (v >> 1) & 0b11;
	}

	const EnumValueDescription EnumPacketFormat[] =
	{
		{ false, "Fixed length (or unlimited)" },
		{ true, "Variable length" }
	};

	const EnumValueDescription EnumDcFree[] =
	{
		{ 0b00, "None (Off)" },
		{ 0b01, "Manchester" },
		{ 0b10, "Whitening" }
	};

	const EnumValueDescription EnumAutoClearOff[] =
	{
		{ false, "Clear FIFO and restart new packet reception. No PayloadReady interrupt issued." },
		{ true, "Do not clear FIFO. PayloadReady interrupt issued." }
	};

	const EnumValueDescription EnumAddressFiltering[] =
	{
		{ 0b00, "None (Off)" },
		{ 0b01, "Address field must match NodeAddress" },
		{ 0b10, "Address field must match NodeAddress or BroadcastAddress" }
	};

	void decodeRegPacketConfig1(uint8_t v)
	{
		bool packetFormat, crcOn, crcAutoClearOff;
		uint8_t dcFree, addressFiltering;
		parseRegPacketConfig1(v, packetFormat, dcFree, crcOn, crcAutoClearOff, addressFiltering);

		cout << "PacketFormat: ";
		printEnum(EnumPacketFormat, 1, packetFormat);
		cout << "\nDcFree: ";
		printEnum(EnumDcFree, 2, dcFree);
		cout << "\nCrcOn: " << crcOn << "\n"
				"CrcAutoClearOff: ";
		printEnum(EnumAutoClearOff, 1, crcAutoClearOff);
		cout << "\nAddressFiltering: ";
		printEnum(EnumAddressFiltering, 2, addressFiltering);
		cout << endl;
	}

	const EnumValueDescription EnumEnterCondition[] =
	{
		{ 0b000, "None (AutoModes Off)" },
		{ 0b001, "Rising edge of FifoNotEmpty" },
		{ 0b010, "Rising edge of FifoLevel" },
		{ 0b011, "Rising edge of CrcOk" },
		{ 0b100, "Rising edge of PayloadReady" },
		{ 0b101, "Rising edge of SyncAddress" },
		{ 0b110, "Rising edge of PacketSent" },
		{ 0b111, "Falling edge of FifoNotEmpty (i.e. FIFO empty)" }
	};

	const EnumValueDescription EnumExitCondition[] =
	{
		{ 0b000, "None (AutoModes Off)" },
		{ 0b001, "Falling edge of FifoNotEmpty (i.e. FIFO empty)" },
		{ 0b010, "Rising edge of FifoLevel or Timeout" },
		{ 0b011, "Rising edge of CrcOk or Timeout" },
		{ 0b100, "Rising edge of PayloadReady or Timeout" },
		{ 0b101, "Rising edge of SyncAddress or Timeout" },
		{ 0b110, "Rising edge of PacketSent" },
		{ 0b111, "Rising edge of Timeout" }
	};

	const EnumValueDescription EnumIntermediateMode[] =
	{
		{ 0b00, "SLEEP" },
		{ 0b01, "STDBY" },
		{ 0b10, "RX" },
		{ 0b11, "TX" }
	};

	void parseAutoRegModes(uint8_t v, uint8_t & enterCondition, uint8_t & exitCondition, uint8_t & intermediateMode)
	{
		enterCondition = v >> 5;
		exitCondition = (v >> 2) & 0b111;
		intermediateMode = v & 0b11;
	}

	void decodeRegAutoModes(uint8_t v)
	{
		uint8_t enterCondition, exitCondition, intermediateMode;
		parseAutoRegModes(v, enterCondition, exitCondition, intermediateMode);

		cout << "EnterCondition: ";
		printEnum(EnumEnterCondition, 3, enterCondition);
		cout << "\nExitCondition: ";
		printEnum(EnumExitCondition, 3, exitCondition);
		cout << "\nIntermediateMode: ";
		printEnum(EnumIntermediateMode, 2, intermediateMode);
		cout << endl;
	}

	void parseRegFifoThresh(uint8_t v, bool & txStartCondition, uint8_t & fifoThreshold)
	{
		txStartCondition = v >> 7;
		fifoThreshold = v & 0b1111111;
	}

	const EnumValueDescription EnumTxStartCondition[] =
	{
		{ false, "FifoLevel" },
		{ true, "FifoNotEmpty" }
	};

	void decodeRegFifoThresh(uint8_t v)
	{
		bool txStartCondition;
		uint8_t fifoThreshold;
		parseRegFifoThresh(v, txStartCondition, fifoThreshold);

		cout << "TxStartCondition: ";
		printEnum(EnumTxStartCondition, 1, txStartCondition);
		cout << "\nFifoThreshold: " << (unsigned)fifoThreshold
				<< " (" << toHex(fifoThreshold, 2) << ", "
				<< toBinary(fifoThreshold, 7) << ')' << endl;
	}

	void parseRegPacketConfig2(uint8_t v, uint8_t & interPacketRxDelay, bool & autoRxRestartOn, bool & aesOn)
	{
		interPacketRxDelay = v >> 4;
		autoRxRestartOn = (v >> 1) & 1;
		aesOn = v & 1;
	}

	void decodeRegPacketConfig2(uint8_t v)
	{
		uint8_t interPacketRxDelay;
		bool autoRxRestartOn, aesOn;
		parseRegPacketConfig2(v, interPacketRxDelay, autoRxRestartOn, aesOn);

		cout << "InterPacketRxDelay: " << (unsigned)interPacketRxDelay << " [";

		if (interPacketRxDelay >= 12)
			cout << "0";
		else
			cout << (2 << interPacketRxDelay) << "/BitRate";

		cout << "]\nAutoRxRestartOn: " << autoRxRestartOn << "\n"
				"AesOn: " << aesOn << endl;
	}

	constexpr uint8_t SENSITIVITY_BOOST_NORMAL = 0x1b;
	constexpr uint8_t SENSITIVITY_BOOST_HIGH = 0x2d;

	const EnumValueDescription EnumSensitivityBoost[] =
	{
		{ SENSITIVITY_BOOST_NORMAL, "Normal mode" },
		{ SENSITIVITY_BOOST_HIGH, "High sensitivity mode" }
	};

	void decodeRegTestLna(uint8_t v)
	{
		cout << "SensitivityBoost: ";
		printEnum(EnumSensitivityBoost, 8, v);
		cout << std::endl;
	}

	const std::pair<uint8_t, DecodeFunctionPtr> SPECIAL_DECODERS[] =
	{
		{ RFM69::RegOpMode, decodeRegOpMode },
		{ RFM69::RegDataModul, decodeRegDataModul },
		{ RFM69::RegOsc1, decodeRegOsc1 },
		{ RFM69::RegAfcCtrl, decodeRegAfcCtrl },
		// TODO: RegListen1
		// TODO: RegListen2
		// TODO: RegListen3
		{ RFM69::RegVersion, decodeRegVersion },
		{ RFM69::RegPaLevel, decodeRegPaLevel },
		{ RFM69::RegPaRamp, decodeRegPaRamp },
		{ RFM69::RegOcp, decodeRegOcp },
		// TODO: receive registers
		{ RFM69::RegDioMapping1, decodeRegDioMapping1 },
		{ RFM69::RegDioMapping2, decodeRegDioMapping2 },
		{ RFM69::RegIrqFlags1, decodeRegIrqFlags1 },
		{ RFM69::RegIrqFlags2, decodeRegIrqFlags2 },
		{ RFM69::RegRssiThresh, decodeRegRssiThresh },
		{ RFM69::RegRxTimeout1, decodeRegRxTimeout1 },
		{ RFM69::RegRxTimeout2, decodeRegRxTimeout2 },
		{ RFM69::RegSyncConfig, decodeRegSyncConfig },
		{ RFM69::RegPacketConfig1, decodeRegPacketConfig1 },
		{ RFM69::RegAutoModes, decodeRegAutoModes },
		{ RFM69::RegFifoThresh, decodeRegFifoThresh },
		{ RFM69::RegPacketConfig2, decodeRegPacketConfig2 },
		{ RFM69::RegTestLna, decodeRegTestLna }
	};
}

DecodeFunctionPtr getDecodeFuntion(uint8_t  r)
{
	const DecodeFunctionPtr * decodeFunction = mapFirstToSecond(SPECIAL_DECODERS, r);
	return decodeFunction ? *decodeFunction : nullptr;
}
