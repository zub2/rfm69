/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "RFM69RegisterMap.h"

#include <algorithm>
#include <cstring>
#include <stdexcept>

#include "rfm69/RFM69.h"
#include "Utils.h"

namespace
{
	#define DEFINE_REG_NAME(x) { #x , RFM69::x }
	static const std::pair<const char *, uint8_t> REGISTER_NAMES[] =
	{
		DEFINE_REG_NAME(RegFifo),
		DEFINE_REG_NAME(RegOpMode),
		DEFINE_REG_NAME(RegDataModul),
		DEFINE_REG_NAME(RegBitrateMsb),
		DEFINE_REG_NAME(RegBitrateLsb),
		DEFINE_REG_NAME(RegFdevMsb),
		DEFINE_REG_NAME(RegFdevLsb),
		DEFINE_REG_NAME(RegFrfMsb),
		DEFINE_REG_NAME(RegFrfMid),
		DEFINE_REG_NAME(RegFrfLsb),
		DEFINE_REG_NAME(RegOsc1),
		DEFINE_REG_NAME(RegAfcCtrl),
		DEFINE_REG_NAME(RegListen1),
		DEFINE_REG_NAME(RegListen2),
		DEFINE_REG_NAME(RegListen3),
		DEFINE_REG_NAME(RegVersion),
		DEFINE_REG_NAME(RegPaLevel),
		DEFINE_REG_NAME(RegPaRamp),
		DEFINE_REG_NAME(RegOcp),
		DEFINE_REG_NAME(RegLna),
		DEFINE_REG_NAME(RegRxBw),
		DEFINE_REG_NAME(RegAfcBw),
		DEFINE_REG_NAME(RegOokPeak),
		DEFINE_REG_NAME(RegOokAvg),
		DEFINE_REG_NAME(RegOokFix),
		DEFINE_REG_NAME(RegAfcFei),
		DEFINE_REG_NAME(RegAfcMsb),
		DEFINE_REG_NAME(RegAfcLsb),
		DEFINE_REG_NAME(RegFeiMsb),
		DEFINE_REG_NAME(RegFeiLsb),
		DEFINE_REG_NAME(RegRssiConfig),
		DEFINE_REG_NAME(RegRssiValue),
		DEFINE_REG_NAME(RegDioMapping1),
		DEFINE_REG_NAME(RegDioMapping2),
		DEFINE_REG_NAME(RegIrqFlags1),
		DEFINE_REG_NAME(RegIrqFlags2),
		DEFINE_REG_NAME(RegRssiThresh),
		DEFINE_REG_NAME(RegRxTimeout1),
		DEFINE_REG_NAME(RegRxTimeout2),
		DEFINE_REG_NAME(RegPreambleMsb),
		DEFINE_REG_NAME(RegPreambleLsb),
		DEFINE_REG_NAME(RegSyncConfig),
		DEFINE_REG_NAME(RegSyncValue1),
		DEFINE_REG_NAME(RegSyncValue2),
		DEFINE_REG_NAME(RegSyncValue3),
		DEFINE_REG_NAME(RegSyncValue4),
		DEFINE_REG_NAME(RegSyncValue5),
		DEFINE_REG_NAME(RegSyncValue6),
		DEFINE_REG_NAME(RegSyncValue7),
		DEFINE_REG_NAME(RegSyncValue8),
		DEFINE_REG_NAME(RegPacketConfig1),
		DEFINE_REG_NAME(RegPayloadLength),
		DEFINE_REG_NAME(RegNodeAdrs),
		DEFINE_REG_NAME(RegBroadcastAdrs),
		DEFINE_REG_NAME(RegAutoModes),
		DEFINE_REG_NAME(RegFifoThresh),
		DEFINE_REG_NAME(RegPacketConfig2),
		DEFINE_REG_NAME(RegAesKey1),
		DEFINE_REG_NAME(RegAesKey2),
		DEFINE_REG_NAME(RegAesKey3),
		DEFINE_REG_NAME(RegAesKey4),
		DEFINE_REG_NAME(RegAesKey5),
		DEFINE_REG_NAME(RegAesKey6),
		DEFINE_REG_NAME(RegAesKey7),
		DEFINE_REG_NAME(RegAesKey8),
		DEFINE_REG_NAME(RegAesKey9),
		DEFINE_REG_NAME(RegAesKey10),
		DEFINE_REG_NAME(RegAesKey11),
		DEFINE_REG_NAME(RegAesKey12),
		DEFINE_REG_NAME(RegAesKey13),
		DEFINE_REG_NAME(RegAesKey14),
		DEFINE_REG_NAME(RegAesKey15),
		DEFINE_REG_NAME(RegAesKey16),
		DEFINE_REG_NAME(RegTemp1),
		DEFINE_REG_NAME(RegTemp2),
		DEFINE_REG_NAME(RegTestLna),
		DEFINE_REG_NAME(RegTestPa1),
		DEFINE_REG_NAME(RegTestPa2),
		DEFINE_REG_NAME(RegTestDagc),
		DEFINE_REG_NAME(RegTestAfc)
	};
	#undef DEFINE_REG_NAME

	uint8_t checkRegisterNumber(unsigned r)
	{
		if (r > 0b1111111)
			throw std::runtime_error("register number out of range, valid numbers are in the range 0..127");

		return static_cast<uint8_t>(r);
	}
}

std::optional<uint8_t> getRFM69RegisterByName(const char * name)
{
	return getRFM69RegisterByName(name, name + strlen(name));
}

std::optional<uint8_t> getRFM69RegisterByName(const char * name, const char *end)
{
	const size_t n = end - name;
	const uint8_t * registerValue = mapFirstToSecondIf(REGISTER_NAMES, [name, n](const auto & rn)
		{
			return strlen(rn.first) == n && !strncmp(name, rn.first, n);
		}
	);
	if (registerValue)
		return *registerValue;

	return std::nullopt;
}

const char * getRFM69RegisterName(uint8_t r)
{
	char const* const* registerName = mapSecondToFirst(REGISTER_NAMES, r);
	return registerName ? *registerName : nullptr;
}

uint8_t parseRegister(const char * s)
{
	const auto r = getRFM69RegisterByName(s);
	if (r)
		return *r;

	return checkRegisterNumber(parseUInt8(s));
}

uint8_t parseRegister(const char * s, const char * end)
{
	const auto r = getRFM69RegisterByName(s, end);
	if (r)
		return *r;

	return checkRegisterNumber(parseUInt8(s, end));
}
