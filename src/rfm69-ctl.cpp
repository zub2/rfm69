/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ft232-test.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdexcept>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <vector>
#include <memory>
#include <optional>

#include <unistd.h>

#include <ftdi.h>

#include "rfm69/RFM69.h"
#include "RFM69RegisterMap.h"
#include "RFM69RegisterDecoders.h"
#include "Command.h"
#include "RFM69SpecialReaders.h"
#include "Utils.h"

namespace
{

const char * PROGRAM_NAME = "rfm69-ctl";

constexpr ftdi_interface DEFAULT_IFACE = INTERFACE_ANY;
// default clock frequency 42 kHz (RFM69 specifies it should be ≤ 10MHz)
constexpr unsigned DEFAULT_SPI_CLOCK = 42000; // Hz

void printHelp()
{
	std::cout << "Usage: " << PROGRAM_NAME << " {IO_SPEC} [-r reg] [-w reg=val] [-d] [-h]\n"
			"\n"
			"Program that can query and control RFM69 RF module.\n"
			"\n"
			"IO_SPEC is one of:\n"
			"\tdevice_name [-i interface] [-c spiClock] - for access via FTDI SPI\n"
			"\t-R read_cmd -W write_cmd - for access via external programs/scrips\n"
			"\n"
			"FTDI SPI options:\n"
			"-i interface\tRequests specific FTDI interface. Must be one of A, B, C or D. "
					"When not specified, ANY is passed to libftdi.\n"
			"-c spiClock\tSPI clock frequency in Hz. Defaults to 42 kHz. RFM69 requires ≤ 10 MHz.\n"
			"\n"
			"The device_name is passed to libftdi's ftdi_usb_open_string. You can use "
			"e.g. 'i:vendor:product' or 'i:vendor:product:index', for example 'i:0x0403:0x6014:1'\n"
			"\n"
			"External programs/scripts options:\n"
			"-R read_cmd\tString passed to shell to read a register. The register number is appended as an argument.\n"
			"-W write_cmd\tString passed to shell to write a register. The register number and value are apended as argumens.\n"
			"\n"
			"Common options:\n"
			"-r reg\tRead value of given register. Use either number (e.g. 1) or name (RegOpMode).\n"
			"-w reg=value\tWrite to given register. Use either number (e.g. 1) or name (RegOpMode). The value can be decadic (12) or hex (0x1b).\n"
			"-d\tDump values of all registers (except for the FIFO). There is no fancy decoding, just a simple diffable dump.\n"
			"-v\tBe more verbose.\n"
			"-h\tPrints this help.\n";
}

class ReadRegisterCommand: public Command
{
public:
	ReadRegisterCommand(const char * s):
		m_r(parseRegister(s))
	{}

	virtual void execute(RFM69 & rfm) override
	{
		const unsigned r = static_cast<unsigned>(m_r);
		const uint8_t v = rfm.readRegister(m_r);
		const unsigned value = v;
		const char * regName = getRFM69RegisterName(m_r);

		std::cout << "register ";

		if (regName)
			std::cout << regName << " (";

		std::cout << toHex(r, 2);

		if (regName)
			std::cout << ')';

		std::cout << ": " << value << " (" << toHex(value, 2) << ", " << toBinary(v, 8) << ")\n" << std::flush;

		DecodeFunctionPtr decodeFunction = getDecodeFuntion(m_r);
		if (decodeFunction)
			decodeFunction(value);
	}

private:
	uint8_t m_r;
};

class WriteRegisterCommand: public Command
{
public:
	WriteRegisterCommand(const char * s)
	{
		std::tie(m_r, m_v) = parseDecription(s);
	}

	virtual void execute(RFM69 & rfm) override
	{
		const char * regName = getRFM69RegisterName(m_r);

		// TODO: support reading of the previous value
		rfm.writeRegister(m_r, m_v);

		const unsigned r = static_cast<unsigned>(m_r);
		std::cout << "setting register ";

		if (regName)
			std::cout << regName << " (";

		std::cout << toHex(r, 2);

		if (regName)
			std::cout << ')';

		std::cout << " to " << static_cast<unsigned>(m_v) << " ("
				<< toHex(m_v, 2) << ", " << toBinary(m_v, 8) << ")\n" << std::flush;
	}

private:
	static std::pair<uint8_t, uint8_t> parseDecription(const char * s)
	{
		const char * p = strchr(s, '=');
		if (!p)
			throw std::runtime_error("can't parse write command argument; use 'portNo=value'");

		const uint8_t r = parseRegister(s, p);
		const uint8_t v = parseUInt8(p+1, p + strlen(p));

		return std::pair{r, static_cast<uint8_t>(v)};
	}

	uint8_t m_r;
	uint8_t m_v;
};

class DumpAllRegisters: public Command
{
public:
	virtual void execute(RFM69 & rfm) override
	{
		std::vector<uint8_t> registerValues = rfm.readRegisters(1, RFM69::RegLast - 1);

		std::cout << "RFM69 registers:\n" << std::hex << std::showbase;

		// don't dump the FIFO (register 0)
		for (unsigned i = 0; i <= registerValues.size(); i++)
			std::cout << "reg[" << i + 1 << "] = " << (unsigned)registerValues[i] << '\n';

		std::cout << std::dec;
	}
};

std::unique_ptr<Command> makeReadCommand(const char * arg)
{
	SpecialReaderFactory factory = getSpecialReaderFactory(arg);
	if (factory)
		return factory();

	return std::make_unique<ReadRegisterCommand>(arg);
}

}

int main(int argc, char ** argv)
{
	if (argc > 0)
		PROGRAM_NAME = argv[0];

	bool verbose = false;

	std::optional<ftdi_interface> iface;
	std::optional<unsigned> spiClockFrequency;
	std::vector<std::unique_ptr<Command>> commands;
	const char * device = nullptr;

	std::string readCommand;
	std::string writeCommand;

	int c;
	while ((c = getopt(argc, argv, "i:c:R:W:r:w:dvh")) != -1)
	{
		switch (c)
		{
		case 'i':
			iface = parseFTDIInterface(optarg);
			break;

		case 'c':
			spiClockFrequency = std::stoul(optarg);
			break;

		case 'R':
			readCommand = optarg;
			break;

		case 'W':
			writeCommand = optarg;
			break;

		case 'r':
			commands.push_back(makeReadCommand(optarg));
			break;

		case 'w':
			commands.push_back(std::make_unique<WriteRegisterCommand>(optarg));
			break;

		case 'd':
			commands.push_back(std::make_unique<DumpAllRegisters>());
			break;

		case 'v':
			verbose = true;
			break;

		case 'h':
			printHelp();
			return EXIT_SUCCESS;

		default:
			std::cerr << "unknown option\n";
			return EXIT_FAILURE;
		}
	}

	if (readCommand.empty() != writeCommand.empty())
	{
		std::cerr << "When using ext IO mode, both -R and -W have to be specified.\n";
		return EXIT_FAILURE;
	}
	const bool useExtIO = !readCommand.empty();

	if (useExtIO)
	{
		if (iface || spiClockFrequency)
		{
			std::cerr << "When using ext IO mode, -i and -c are meaningless.\n";
			return EXIT_FAILURE;
		}
	}
	else
	{
		if (optind == argc)
		{
			std::cerr << "Missing required argument: libftdi device name\n";
			return EXIT_FAILURE;
		}
		device = argv[optind];
		optind++;
	}
	if (argc > optind)
	{
		std::cerr << "Too many arguments.\n";
		return EXIT_FAILURE;
	}

	if (commands.empty())
	{
		std::cerr << "No command specified. Use one or more of -r and/or -w commands.\n";
		return EXIT_FAILURE;
	}

	// power level is not set here, so just specify highPower as false
	RFM69 rfm69 = useExtIO ?
			RFM69(std::move(readCommand), std::move(writeCommand), false, verbose) :
			RFM69(device, iface.value_or(DEFAULT_IFACE), spiClockFrequency.value_or(DEFAULT_SPI_CLOCK), false, verbose);

	for (size_t i = 0; i < commands.size(); i++)
	{
		if (i != 0)
			std::cout.put('\n');

		auto & command = commands[i];
		command->execute(rfm69);
	}

	return EXIT_SUCCESS;
}
