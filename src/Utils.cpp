/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Utils.h"

#include <limits>
#include <sstream>
#include <iomanip>
#include <stdexcept>
#include <vector>
#include <cstring>

ftdi_interface parseFTDIInterface(const char * i)
{
	if (strlen(i) == 1)
	{
		switch (*i)
		{
		case 'A':
		case 'a':
				return INTERFACE_A;

		case 'B':
		case 'b':
				return INTERFACE_B;

		case 'C':
		case 'c':
				return INTERFACE_C;

		case 'D':
		case 'd':
				return INTERFACE_D;
		}
	}

	throw std::runtime_error("Wrong interface argument. Use one of A, B, C or D.");
}

uint8_t parseUInt8(const std::string & s)
{
	size_t pos;
	const unsigned long long n = std::stoull(s, &pos, 0);

	// there can be no other character (not even a whitespace) after the number
	if (pos != s.length())
		throw std::invalid_argument("argument is not a number");

	if (n > std::numeric_limits<uint8_t>::max())
		throw std::out_of_range(std::string("value ") + std::to_string(n) + " can't fit in uint8");

	return static_cast<uint8_t>(n);
}

uint8_t parseUInt8(const char * begin, const char * end)
{
	return parseUInt8(std::string(begin, end - begin));
}

uint8_t parseUInt8(const char * s)
{
	return parseUInt8(std::string(s));
}

std::string toBinary(unsigned n, unsigned nbits)
{
	std::vector<char> bits;
	bits.reserve(nbits);
	while (nbits--)
	{
		bits.push_back(n & 1 ? '1' : '0');
		n = n >> 1;
	}

	std::stringstream s;

	s << "0b";
	for (auto ri = bits.rbegin(); ri != bits.rend(); ri++)
		s << *ri;

	return s.str();
}

std::string toHex(unsigned n, unsigned nchars)
{
	std::stringstream s;

	s << "0x" << std::setw(nchars) << std::setfill('0') << std::hex << n;

	return s.str();
}

void printBytes(std::ostream &os, const uint8_t * bytes, size_t size)
{
	for (size_t i = 0; i < size; i++)
	{
		if (i != 0)
			os << ',';

		os << toHex(bytes[i], 2);
	}
}

void printBytes(std::ostream &os, const std::vector<uint8_t> & bytes)
{
	printBytes(os, bytes.data(), bytes.size());
}
