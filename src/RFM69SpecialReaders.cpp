/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "RFM69SpecialReaders.h"

#include <cstring>
#include <algorithm>
#include <iostream>
#include <sstream>

#include "Utils.h"

namespace
{
	using SpecialReaderFactoryDefinition = std::pair<const char *, SpecialReaderFactory>;

	template<typename T>
	std::unique_ptr<Command> readerFactory()
	{
		return std::make_unique<T>();
	}

	std::string formatUnit(float value, const char * unit)
	{
		std::stringstream s;

		constexpr float MEGA = 1e6f;
		constexpr float KILO = 1e3f;

		if (value > MEGA)
			s << (value / MEGA) << " M";
		else if (value > KILO)
			s << (value / KILO) << " k";
		else
			s << value << ' ';

		s << unit;

		return s.str();
	}

	class RegBitrateReader : public Command
	{
	public:
		virtual void execute(RFM69 & rfm) override
		{
			const uint8_t bitrateMsb = rfm.readRegister(RFM69::RegBitrateMsb);
			const uint8_t bitrateLsb = rfm.readRegister(RFM69::RegBitrateLsb);

			const uint16_t bitrate = (bitrateMsb << 8) | bitrateLsb;
			const unsigned bitrateHz = RFM69::FXOSC / bitrate;

			std::cout << "RegBitrate: " << bitrate << " (" << toHex(bitrate, 4) << ") "
					"[" << formatUnit(bitrateHz, "b/s") << ']' << std::endl;
		}
	};

	class RegFdevReader : public Command
	{
	public:
		virtual void execute(RFM69 & rfm) override
		{
			const uint8_t fdevMsb = rfm.readRegister(RFM69::RegFdevMsb);
			const uint8_t fdevLsb = rfm.readRegister(RFM69::RegFdevLsb);

			const uint16_t fdev = ((fdevMsb & 0b00111111) << 8) | fdevLsb;
			const float fdevHz = RFM69::FSTEP * fdev;

			std::cout << "RegFdev: " << fdev << " (" << toHex(fdev, 4) << ") "
					"[" << formatUnit(fdevHz, "Hz") << ']' << std::endl;
		}
	};

	class RegFrfReader : public Command
	{
	public:
		virtual void execute(RFM69 & rfm) override
		{
			const uint8_t frfMsb = rfm.readRegister(RFM69::RegFrfMsb);
			const uint8_t frfMid = rfm.readRegister(RFM69::RegFrfMid);
			const uint8_t frfLsb = rfm.readRegister(RFM69::RegFrfLsb);

			const uint32_t frf = (frfMsb << 16) | (frfMid << 8) | frfLsb;
			const float frfHz = RFM69::FSTEP * frf;

			std::cout << "RegFrf: " << frf << " (" << toHex(frf, 6) << ") "
					"[" << formatUnit(frfHz, "Hz") << ']' << std::endl;
		}
	};

	class RegPreambleMsbReader : public Command
	{
	public:
		virtual void execute(RFM69 & rfm) override
		{
			const uint8_t preambleMsb = rfm.readRegister(RFM69::RegPreambleMsb);
			const uint8_t preambleLsb = rfm.readRegister(RFM69::RegPreambleLsb);

			const uint32_t preamble = (preambleMsb << 8) | preambleLsb;

			std::cout << "RegPreamble: " << preamble << " (" << toHex(preamble, 4) << ')' << std::endl;
		}
	};

	class RegSyncValueReader : public Command
	{
	public:
		virtual void execute(RFM69 & rfm) override
		{
			const std::vector<uint8_t> syncValues = rfm.readRegisters(RFM69::RegSyncValue1, 8);

			std::cout << "RegSyncValue: ";
			for (size_t i = 0; i < syncValues.size(); i++)
			{
				if (i != 0)
					std::cout << ", ";

				std::cout << toHex(syncValues[i], 2);
			}
			std::cout << std::endl;
		}
	};

	class RegAesKeyReader : public Command
	{
	public:
		virtual void execute(RFM69 & rfm) override
		{
			const std::vector<uint8_t> aesKey = rfm.readRegisters(RFM69::RegAesKey1, 8);

			std::cout << "RegAesKey: ";
			for (size_t i = 0; i < aesKey.size(); i++)
			{
				if (i != 0)
					std::cout << ", ";

				std::cout << toHex(aesKey[i], 2);
			}
			std::cout << std::endl;
		}
	};

	const SpecialReaderFactoryDefinition SPECIAL_READER_FACTORIES[] =
	{
		{ "RegBitrate", readerFactory<RegBitrateReader> },
		{ "RegFdev", readerFactory<RegFdevReader> },
		{ "RegFrf", readerFactory<RegFrfReader> },
		{ "RegPreamble", readerFactory<RegPreambleMsbReader> },
		{ "RegSyncValue", readerFactory<RegSyncValueReader> },
		{ "RegAesKey", readerFactory<RegAesKeyReader> }
	};
}

SpecialReaderFactory getSpecialReaderFactory(const char * name)
{
	const SpecialReaderFactory * factory = mapFirstToSecondIf(SPECIAL_READER_FACTORIES, [name](const auto & f)
		{
			return !strcmp(name, f.first);
		}
	);

	return factory ? *factory : nullptr;
}
