/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RFM69_UTILS_H
#define RFM69_UTILS_H

#include <cstdint>
#include <string>
#include <utility>
#include <algorithm>
#include <ostream>
#include <vector>

#include <ftdi.h>

ftdi_interface parseFTDIInterface(const char * i);

uint8_t parseUInt8(const std::string & s);
uint8_t parseUInt8(const char * begin, const char * end);
uint8_t parseUInt8(const char * s);

std::string toBinary(unsigned n, unsigned nbits);
std::string toHex(unsigned n, unsigned nchars);

void printBytes(std::ostream &os, const uint8_t * bytes, size_t size);
void printBytes(std::ostream &os, const std::vector<uint8_t> & bytes);

template<typename T, typename U, size_t N, typename Predicate>
const U * mapFirstToSecondIf(const std::pair<T, U> (&map)[N], Predicate && predicate)
{
	using Pair = std::pair<T, U>;
	const Pair * begin = std::begin(map);
	const Pair * end = std::end(map);

	const Pair * p = std::find_if(begin, end, predicate);

	return p != end ? &(p->second) : nullptr;
}

template<typename T, typename U, size_t N>
const U * mapFirstToSecond(const std::pair<T, U> (&map)[N], const T & first)
{
	return mapFirstToSecondIf(map, [&first](const std::pair<T, U> & p)
		{
			return p.first == first;
		}
	);
}

template<typename T, typename U, size_t N>
const T * mapSecondToFirst(const std::pair<T, U> (&map)[N], const U & second)
{
	using Pair = std::pair<T, U>;
	const Pair * begin = std::begin(map);
	const Pair * end = std::end(map);

	const Pair * p = std::find_if(begin, end, [&second](const Pair & p)
		{
			return p.second == second;
		}
	);

	return p != end ? &(p->first) : nullptr;
}

#endif // RFM69_UTILS_H
