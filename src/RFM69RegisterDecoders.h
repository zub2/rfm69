/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rfm69.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef RFM69_RFM69REGISTER_DECODERS_H
#define RFM69_RFM69REGISTER_DECODERS_H

#include <cstdint>

using DecodeFunctionPtr = void(*)(uint8_t);

DecodeFunctionPtr getDecodeFuntion(uint8_t  r);

#endif // RFM69_RFM69REGISTER_DECODERS_H
