/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of rfm69.
 *
 * rfm69 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rfm69 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ft232-test.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This is inspired by:
 * * https://pushstack.wordpress.com/2019/03/02/using-semtech-sx1231-to-transmit-custom-frames/
 * * https://github.com/dimhoff/sx1231_ods/blob/master/libsx1231_ods/spi.c
 */

#include <stdexcept>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <vector>
#include <memory>
#include <charconv>
#include <chrono>
#include <cctype>
#include <optional>

#include <unistd.h>

#include <ftdi.h>

#include "rfm69/RFM69.h"
#include "rfm69/RFM69OOKTransmitter.h"
#include "Utils.h"

namespace
{

const char * PROGRAM_NAME = "rfm69-ook-transmit";

void printHelp()
{
	std::cout << "Usage: " << PROGRAM_NAME << " [-i interface] [-c spiClock] [-f frequency] [-H] [-P powerLevel] [-T bitDuration] [-F packetFormat] [-p preambleBytes] [-s syncBytes] [-v] [-d] [-h] device_name payload_bytes\n"
			"\n"
			"Program that can transmit a packet in OOK modulation using a RFM69 module.\n"
			"\n"
			"-i interface\tRequests specific FTDI interface. Must be one of A, B, C or D. "
					"When not specified, ANY is passed to libftdi.\n"
			"-c spiClock\tSPI clock frequency in Hz. Defaults to 42 kHz. RFM69 requires ≤ 10 MHz.\n"
			"-f frequency\tTransmit frequency in HZ. Defaults to 433 MHz.\n"
			"-H\tThe RFM69 module is 'high power' (RFM69HW, RFM69HCW). Required for high-power devices otherwise nothing is broadcast.\n"
			"-P powerLevel\tOutput power level in dBm. -18..+13 for non-H devices, -2..+20 for H devices. Defaults to +13.\n"
			"-T bitDuration\tBit duration in µs. Defaults to 100 µs. Max value is ~2ms.\n"
			"-F packetFormat\tOne of fixed, variable or unlimited. Defaults to unlimited.\n"
			"-p preambleBytes\tNumber of preamble bytes which are transmitted before the sync bytes and the payload. The bytes "
			"transmitted are 0xaa, with the exception that specifying 0 produces exactly 1 high bit. Defaults to 0.\n"
			"-s\tSync bytes. 0 to 8 bytes. Defaults to 0 bytes.\n"
			"-v\tBe more verbose.\n"
			"-d\tDry run.\n"
			"-h\tPrints this help.\n"
			"\n"
			"device_name\t Name passed to libftdi's ftdi_usb_open_string. You can use "
			"e.g. 'i:vendor:product' or 'i:vendor:product:index', for example 'i:0x0403:0x6014:1'\n"
			"payload_bytes\tBytes to send. Use number separated by commas, e.g. 1,2,3,0xff,0\n";
}

std::vector<uint8_t> parseBytes(const char * s)
{
	std::vector<uint8_t> bytes;
	while (*s)
	{
		while (*s && isspace(*s))
			s++;

		const char *numberStart = s;
		while (*s && (isxdigit(*s) || *s == 'x'))
			s++;

		bytes.push_back(parseUInt8(numberStart, s));

		while (*s && isspace(*s))
			s++;

		if (*s)
		{
			if (*s != ',')
				throw std::runtime_error("can't parse input bytes");

			s++;
		}
	}

	return bytes;
}

enum class PacketFormat
{
	fixed,
	variable,
	unlimited
};

PacketFormat parsePacketFormat(const char * s)
{
	if (strcmp(s, "fixed") == 0)
		return PacketFormat::fixed;
	else if (strcmp(s, "variable") == 0)
		return PacketFormat::variable;
	else if (strcmp(s, "unlimited") == 0)
		return PacketFormat::unlimited;

	throw std::runtime_error("Packet format must be one of: 'fixed', 'variable' or 'unlimited'.");
}

std::ostream & operator<< (std::ostream & os, PacketFormat packetFormat)
{
	switch (packetFormat)
	{
	case PacketFormat::fixed:
		os << "fixed";
		break;

	case PacketFormat::unlimited:
		os << "unlimited";
		break;

	case PacketFormat::variable:
		os << "variable";
		break;
	}

	return os;
}

}

int main(int argc, char ** argv)
{
	if (argc > 0)
			PROGRAM_NAME = argv[0];

	ftdi_interface iface = INTERFACE_ANY;
	unsigned spiClockFrequency = 42000; // Hz
	unsigned frequency = 433000000; // Hz
	bool highPower = false;
	int powerLevel = 13; // dBm (this is the default for RFM69W, RFM69CW)
	std::chrono::microseconds bitDuration(100);
	PacketFormat packetFormat = PacketFormat::unlimited;
	unsigned preambleSize = 0;
	std::vector<uint8_t> syncBytes;
	bool verbose = false;
	bool dryRun = false;

	int c;
	while ((c = getopt(argc, argv, "i:c:f:HP:T:F:p:s:vdh")) != -1)
	{
		switch (c)
		{
		case 'i':
			iface = parseFTDIInterface(optarg);
			break;

		case 'c':
			spiClockFrequency = std::stoul(optarg);
			break;

		case 'f':
			frequency = std::stoul(optarg);
			break;

		case 'H':
			highPower = true;
			break;

		case 'P':
			powerLevel = std::stoi(optarg);
			break;

		case 'T':
			bitDuration = std::chrono::microseconds(std::stoul(optarg));
			break;

		case 'F':
			packetFormat = parsePacketFormat(optarg);
			break;

		case 'p':
			preambleSize = std::stoul(optarg);
			break;

		case 's':
			syncBytes = parseBytes(optarg);
			break;

		case 'v':
			verbose = true;
			break;

		case 'd':
			dryRun = true;
			break;

		case 'h':
			printHelp();
			return EXIT_SUCCESS;

		default:
			std::cerr << "unknown option\n";
			return EXIT_FAILURE;
		}
	}

	if (argc > optind + 3)
	{
		std::cerr << "Too many arguments.\n";
		return EXIT_FAILURE;
	}

	if (optind == argc)
	{
		std::cerr << "Missing required argument: libftdi device name\n";
		return EXIT_FAILURE;
	}
	const char * device = argv[optind];

	std::vector<uint8_t> bytes;
	if (optind + 1 == argc && !dryRun)
	{
		std::cerr << "Missing required argument: bytes\n";
		return EXIT_FAILURE;
	}
	if (optind + 1 < argc)
	{
		bytes = parseBytes(argv[optind + 1]);
	}

	if (syncBytes.size() > 8)
	{
		std::cerr << "Too many header bytes. RFM69 limits header size to 8 bytes.\n";
		return EXIT_FAILURE;
	}

	RFM69 rfm69(device, iface, spiClockFrequency, highPower, verbose);

	auto [revision, maskRevision] = rfm69.getVersion();
	std::cout << "RFM69 rev. " << revision << " (mask rev. " << maskRevision << ")\n";

	if (revision == 0)
	{
		std::cerr << "RFM69 is probably not connected (rev == 0 is unexpected)\n";
		return EXIT_FAILURE;
	}

	std::optional<uint8_t> payloadLength;
	if (packetFormat != PacketFormat::unlimited)
	{
		payloadLength = bytes.size();
		// TODO: check size limits
	}

	RFM69OOKTransmitter transmitter(rfm69, frequency, bitDuration, payloadLength, preambleSize, syncBytes);

	if (!bytes.empty())
	{
		if (dryRun)
			std::cout << "Would broadcast:";
		else
			std::cout << "Broadcasting:";

		std::cout << "\n\t* packet format: " << packetFormat;

		if (packetFormat != PacketFormat::unlimited)
			std::cout << " (" << bytes.size() << " bytes)";

		std::cout << "\n\t* preamble: ";
		if (preambleSize == 0)
			std::cout << "1 bit (1)";
		else
			std::cout << preambleSize << " byte(s) (0xaa)";

		std::cout << "\n\t* sync word: ";
		if (syncBytes.empty())
			std::cout << "none";
		else
			printBytes(std::cout, syncBytes);

		std::cout << "\n\t* payload: ";
		printBytes(std::cout, bytes);
		std::cout << std::endl;

		if (!dryRun)
		{
			rfm69.setPowerLevel(powerLevel);
			transmitter.broadcast(bytes.data(), bytes.size());
		}
	}

	return EXIT_SUCCESS;
}
